import { GET_MENU_ITEMS } from '../types'

const initialState = {
    items: []
}

const menuReducer = (state = initialState, { type, payload }) => {
    switch (type) {

    case GET_MENU_ITEMS:
        // const { user } = payload.user
        return { ...state, ...payload }

    default:
        return state
    }
}

export default menuReducer