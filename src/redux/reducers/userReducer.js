import {
	// Login
	USER_LOGIN_ERROR,
	USER_LOGIN_SUCCESS,
	// Register
	USER_REGISTER_LOADING,
	USER_REGISTER_ERROR,
	USER_REGISTER_SUCCESS,
	// Current User
	SET_USER_INFO_SUCCESS,
	USER_LOGOUT_SUCCESS,
} from '../types'

import config from '../../config'

const initialState = {
	currentUser: null,
	errorCurrentUser: false,
	errorLogin: false,
	errorLogout: false,
	errorRegister: false,
	token: ''
}

const userReducer = (state = initialState, action) => {
	state.token = localStorage.getItem(config.authToken) || ''

	switch (action.type) {
		// Login
		case USER_LOGIN_ERROR:
			return {
				...initialState,
				errorLogin: action.payload.error,
			}
		case USER_LOGIN_SUCCESS:
			return {
				...initialState,
				token: action.payload.token,
			}
		// Register
		case USER_REGISTER_LOADING:
			return {
				...initialState,
				loadingRegister: true,
			}
		case USER_REGISTER_ERROR:
			return {
				...initialState,
				errorRegister: action.payload.error,
			}
		case USER_REGISTER_SUCCESS:
			return {
				...initialState,
				currentUser: action.payload.user,
			}
		// Set User Info
		case SET_USER_INFO_SUCCESS:
			return {
				...state,
				currentUser: action.payload.user,
			}
		// Logout
		case USER_LOGOUT_SUCCESS:
			return {
				...initialState,
				token: ''
			}
		default:
			return state
	}
}

export default userReducer
