import { createStore, compose, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import menuReducer from './reducers/menuReducer'
import userReducer from './reducers/userReducer'

const initialState = {}
const composeEnhacer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
	combineReducers({
		user: userReducer,
		menu: menuReducer,
	}),
	initialState,
	composeEnhacer(applyMiddleware(thunk))
)

export default store
