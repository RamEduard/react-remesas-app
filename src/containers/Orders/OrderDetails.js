import React from 'react'
import { useHistory, useParams } from 'react-router'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { makeStyles, Button, Container, Grid, Typography, Paper, Stepper, Step, StepLabel } from '@material-ui/core'
import { Add as AddIcon, Cached as CachedIcon } from '@material-ui/icons'
import { get, isEmpty } from 'lodash'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import CustomDialogComponent from '../../components/Dialog/CustomDialog'
import Fare from '../../components/Fare'
import Loading from '../../components/Loading'
import OrderForm from '../../components/Order/OrderForm'
import TransactionForm from '../../components/Transaction/TransactionForm'
import TransactionsTable from '../../components/Transaction/TransactionsTable'
import ArbitrageTransactionForm from '../../components/Arbitrage/ArbitrageTransactionForm'
import ArbitrageTransactionsTable from '../../components/Arbitrage/ArbitrageTransactionsTable'

// Queries
import { GET_ARBITRAGE_TRANSACTION_LIST, GET_ORDER } from '../../graphql/queries'
import {
	ARBITRAGE_TRANSACTION_CREATE,
	ARBITRAGE_TRANSACTION_DELETE,
	ARBITRAGE_TRANSACTION_UPDATE,
	ORDER_UPDATE,
	TRANSACTION_CREATE,
	TRANSACTION_DELETE,
	TRANSACTION_UPDATE,
} from '../../graphql/mutations'

const TEMP_CURRENCIES = ['ARS', 'CLP', 'COP', 'EUR', 'PEN', 'USD', 'VES', 'MXN', 'BRL']

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map((c) => ({
	label: c,
	value: c,
}))

const useStyles = makeStyles((theme) => ({
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
	paper: {
		padding: theme.spacing(2),
	},
}))

const OrderDetailContainer = () => {
	const classes = useStyles()
	const history = useHistory()
	const { orderId } = useParams()

	// States
	const [openDialog, setOpenDialog] = React.useState()
	const [openDialogArbitrage, setOpenDialogArbitrage] = React.useState()

	// Order states
	const [orderValues, setOrderValues] = React.useState({})
	// Transactions states
	const [transactions, setTransactions] = React.useState([])
	const [transactionValues, setTransactionValues] = React.useState({})
	const [isEditTransaction, setIsEditTransaction] = React.useState()
	const [isRemoveTransaction, setIsRemoveTransaction] = React.useState()
	const [pathTransactionDetails, setPathTransactionDetails] = React.useState() // TODO: Usar para Orden asociada y usuario
	// Arbitrage transactions
	const [arbitrageTransactions, setArbitrageTransactions] = React.useState([])
	const [arbitrageTransactionValues, setArbitrageTransactionValues] = React.useState({})
	const [isEditArbitrageTransaction, setIsEditArbitrageTransaction] = React.useState()
	const [isRemoveArbitrageTransaction, setIsRemoveArbitrageTransaction] = React.useState()
	const [transactionOptions, setTransactionOptions] = React.useState([])

	// Apollo hooks
	const [getOrder, { data: dataOrder, loading: loadingOrder, refetch }] = useLazyQuery(GET_ORDER)
	const [getArbitrageTxs, { data: dataArbitrageTxs, loading: loadingArbitrageTxs, refetch: refetchArbitrageTxs }] = useLazyQuery(GET_ARBITRAGE_TRANSACTION_LIST)
	const [updateOrder, { data: updatedOrder, loading: loadingUpdateOrder }] = useMutation(ORDER_UPDATE)
	const [createTransaction, { data: createdTransaction, loading: loadingCreateTx }] = useMutation(TRANSACTION_CREATE)
	const [deleteTransaction, { data: deletedTransaction, loading: loadingDeleteTx }] = useMutation(TRANSACTION_DELETE)
	const [updateTransaction, { data: updatedTransaction, loading: loadingUpdateTx }] = useMutation(TRANSACTION_UPDATE)
	const [createArbitrageTransaction, { data: createdArbitrageTx, loading: loadingCreateArbitrageTx }] = useMutation(
		ARBITRAGE_TRANSACTION_CREATE
	)
	const [deleteArbitrageTransaction, { data: deletedArbitrageTx, loading: loadingDeleteArbitrageTx }] = useMutation(
		ARBITRAGE_TRANSACTION_DELETE
	)
	const [updateArbitrageTransaction, { data: updatedArbitrageTx, loading: loadingUpdateArbitrageTx }] = useMutation(
		ARBITRAGE_TRANSACTION_UPDATE
	)

	React.useEffect(() => {
		if (orderId && orderId !== '') {
			getOrder({ variables: { _id: orderId } })
			getArbitrageTxs({ variables: { filters: { orderId } } })
		}
	}, [orderId])

	// Al obtener los datos de la orden
	React.useEffect(() => {
		const _order = get(dataOrder, 'orderGet')
		const _transactions = get(dataOrder, 'orderGet.transactions') || []
		if (!isEmpty(_order)) {
			setTransactions(_transactions)
			const { token, transactions, user, userClient, createdAt, updatedAt, ...orderVals } = _order
			setOrderValues(orderVals)
		}
		return () => {
			setOrderValues({})
			setTransactions([])
		}
	}, [dataOrder])

	React.useEffect(() => {
		// Setear transactionOptions
		if (transactions.length > 0) {
			setTransactionOptions(
				transactions.map(t => ({
					type: t.type,
					label: `${t.type} -- ${Fare({ amount: t.amountCurrency, symbol: t.currency })} / ${Fare({ amount: t.rate, symbol: t.currency })} => BTC ${t.amountBtc}`,
					amountBtc: t.amountBtc,
					value: t._id
				}))
			)
		}
	}, [transactions])

	React.useEffect(() => {
		const _items = get(dataArbitrageTxs, 'arbitrageTransactionsList.items') || []
		if (!isEmpty(_items)) {
			setArbitrageTransactions(_items)
		}
	}, [dataArbitrageTxs])

	// Cuando se ha creado la transacción
	React.useEffect(() => {
		const _created = get(createdTransaction, 'transactionCreate')
		if (_created && _created._id) {
			// history.push(`/transaction/${_created._id}`)
			setTransactions((prevItems) => (!prevItems.length ? [_created] : [...prevItems, _created]))
		}
	}, [createdTransaction])

	// Cuando se actualiza una transacción
	React.useEffect(() => {
		const _updated = get(updatedTransaction, 'transactionUpdate')
		if (_updated) {
			setTransactions((prevItems) => {
				return prevItems.map((i) => {
					if (i._id === _updated._id) {
						i = _updated
					}
					return i
				})
			})
		}
	}, [updatedTransaction])

	// Cuando se elimina
	React.useEffect(() => {
		const _deleted = get(deletedTransaction, 'transactionDelete')
		if (_deleted && transactionValues) {
			setTransactions((prevItems) => prevItems.filter((i) => i._id !== transactionValues._id))
			setTransactionValues()
		}
	}, [deletedTransaction, transactionValues])

	/**
	 * Recargar tabla de transacciones
	 */
	const onHandleRefresh = () => {
		refetch({ variables: { _id: orderId } })
		refetchArbitrageTxs({ variables: { orderId }})
	}

	/**
	 * Cerrar modal de agregar campaña
	 */
	const onHandleCloseDialog = () => {
		setOpenDialog(false)
		setIsEditTransaction()
		setTransactionValues()
		setPathTransactionDetails()
		setIsRemoveTransaction()
	}

	/**
	 * Al hacer click en una fila de tabla de transacciones
	 *
	 * @param {object} row
	 */
	const onHandleClickRow = (row) => {
		// Editar en modal
		setIsEditTransaction(true)

		// Quitar algunos campos
		const { user, createdAt, updatedAt, ...values } = row

		setTransactionValues(values)
		setOpenDialog(true)
		setPathTransactionDetails(`/transaction/${row._id}`)
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	const onHandleSubmitOrder = (values) => {
		// TODO: Buscar base rate
		if (values.fromAmount && values.toAmount) {
			values.baseRate = Math.round((values.toAmount / values.fromAmount) * 100) / 100
		}

		// console.log('OrderForm Values:', values)

		// Con los valores seteados por AutoSubmit los enviamos a Backend
		updateOrder({
			variables: { input: values },
		})
	}

	/**
	 * Abrir modal para agregar nueva transacción
	 */
	const onClickNewTransaction = () => {
		setOpenDialog(true)
		setTransactionValues({
			amountCurrency: '',
			amountBtc: '',
			currency: '',
			currencyLabel: '',
			orderId,
			paymentMethod: '',
			paymentMethodDescription: '',
			rate: '',
			type: '',
		})
	}

	/**
	 * Redireccionar a detalles de campaña
	 */
	const onHandleTransactionDetails = () => {
		history.push(pathTransactionDetails)
		onHandleCloseDialog()
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	const onHandleSubmitTransaction = (values) => setTransactionValues(values)

	/**
	 * Mostrar modal de confirmación de eliminar campaña
	 */
	const onHandleRemoveTransaction = () => {
		setIsEditTransaction(false)
		setIsRemoveTransaction(true)
	}

	/**
	 * Confirmación de eliminar el campaña del Backend
	 */
	const onHandleConfirmRemoveTransaction = () => {
		// Eliminar del backend
		deleteTransaction({
			variables: {
				_id: transactionValues._id,
			},
		})

		setOpenDialog(false)
		setIsEditTransaction()
		setPathTransactionDetails()
		setIsRemoveTransaction()
	}

	/**
	 * Guardar valores del formulario con GraphQL
	 */
	const onHandleSaveTransaction = () => {
		// Nuevo contacto
		if (!isEditTransaction && transactionValues) {
			// console.log('TransactionForm Values:', transactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			createTransaction({
				variables: { input: transactionValues },
			})
		}
		// Editar contacto
		else if (isEditTransaction && transactionValues) {
			// console.log('TransactionForm Values:', transactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			updateTransaction({
				variables: { input: transactionValues },
			})
		}

		onHandleCloseDialog()
	}

	/**
	 * Al hacer click en una fila de tabla de transacciones arbitrage
	 *
	 * @param {object} row
	 */
	const onHandleClickRowArbitrage = (row) => {
		// Editar en modal
		setIsEditArbitrageTransaction(true)

		// Quitar algunos campos
		const { createdAt, updatedAt, ...values } = row

		setArbitrageTransactionValues(values)
		setOpenDialogArbitrage(true)
	}

	/**
	 * Cerrar modal de agregar arbitrage
	 */
	const onHandleCloseDialogArbitrageTx = () => {
		setOpenDialogArbitrage(false)
		setIsEditArbitrageTransaction()
		setArbitrageTransactionValues()
		setIsRemoveArbitrageTransaction()
	}

	/**
	 * Abrir modal para agregar nueva transacción
	 */
	const onClickNewArbitrageTx = () => {
		setOpenDialogArbitrage(true)
		setArbitrageTransactionValues({
			orderId,
			service: 'LocalBitcoins',
			profitBtc: '',
			profitPercent: '',
			buyTransactionId: '',
			sellTransactionId: '',
		})
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	const onHandleSubmitArbitrageTx = (values) => setArbitrageTransactionValues(values)

	/**
	 * Mostrar modal de confirmación de eliminar arbitrage
	 */
	const onHandleRemoveArbitrageTx = () => {
		setIsEditArbitrageTransaction(false)
		setIsRemoveArbitrageTransaction(true)
	}

	/**
	 * Confirmación de eliminar el campaña del Backend
	 */
	const onHandleConfirmRemoveArbitrageTx = () => {
		// Eliminar del backend
		deleteArbitrageTransaction({
			variables: {
				_id: arbitrageTransactionValues._id,
			},
		})

		setOpenDialogArbitrage(false)
		setIsEditArbitrageTransaction()
		setIsRemoveArbitrageTransaction()
	}

	/**
	 * Guardar valores del formulario con GraphQL
	 */
	const onHandleSaveArbitrageTx = () => {
		// Nuevo contacto
		if (!isEditArbitrageTransaction && arbitrageTransactionValues) {
			// console.log('ArbitrageTransactionForm Values:', arbitrageTransactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			createArbitrageTransaction({
				variables: { input: arbitrageTransactionValues },
			})
		}
		// Editar contacto
		else if (isEditArbitrageTransaction && arbitrageTransactionValues) {
			// console.log('ArbitrageTransactionForm Values:', arbitrageTransactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			updateArbitrageTransaction({
				variables: { input: arbitrageTransactionValues },
			})
		}

		onHandleCloseDialogArbitrageTx()
	}

	return (
		<Container maxWidth="xl">
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
						path: '/orders',
						label: 'Orders',
					},
					{
						path: '#',
						label: 'Order details',
					},
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={3} md={4} xs={12}>
					{loadingOrder && <Loading />}
					<Paper className={classes.paper}>
						<Typography component="h2" variant="h5">
							Order details
						</Typography>
						<OrderForm
							initialValues={orderValues}
							currencyOptions={CURRENCIES_ITEMS}
							onHandleSubmit={onHandleSubmitOrder}
							onHandleCancel={() => history.goBack()}
						/>
					</Paper>
				</Grid>
				<Grid item lg={9} md={8} xs={12}>
					<Typography component="h2" variant="h5">
						Transactions related to
					</Typography>
					<Button variant="outlined" onClick={onHandleRefresh}>
						<CachedIcon /> Refresh
					</Button>
					<Button variant="outlined" onClick={onClickNewTransaction}>
						<AddIcon /> New
					</Button>
					{/* Transactions table */}
					<TransactionsTable onHandleClickRow={onHandleClickRow} data={transactions} />

					<br />
					{loadingArbitrageTxs && <Loading />}
					<Typography component="h2" variant="h5">
						Arbitrage related to
					</Typography>
					<Button variant="outlined" onClick={onHandleRefresh}>
						<CachedIcon /> Refresh
					</Button>
					<Button variant="outlined" onClick={onClickNewArbitrageTx}>
						<AddIcon /> New
					</Button>
					{/* Transactions table */}
					<ArbitrageTransactionsTable data={arbitrageTransactions} onHandleClickRow={onHandleClickRowArbitrage} />
					{/* Nuevo / Editar / Eliminar Transaction Modal */}
					<CustomDialogComponent
						open={openDialog}
						onHandleClose={onHandleCloseDialog}
						titleText={
							isEditTransaction
								? 'Transaction details'
								: isRemoveTransaction
								? 'Remove transaction'
								: 'Add new transaction'
						}
						renderContent={
							isRemoveTransaction ? (
								<div>
									<p>Esta acción que estás a punto de hacer no es reversible. ¿Deseas continuar?</p>
								</div>
							) : (
								<TransactionForm
									autosubmit
									disabledInputs={isEditTransaction ? {} : {}}
									initialValues={transactionValues}
									loading={loadingCreateTx || loadingUpdateTx}
									onHandleSubmit={onHandleSubmitTransaction}
								/>
							)
						}
						renderFooter={
							<>
								{isEditTransaction && pathTransactionDetails && (
									<>
										<Button variant="outlined" type="button" onClick={onHandleTransactionDetails}>
											More details
										</Button>
										<Button color="secondary" variant="contained" type="button" onClick={onHandleRemoveTransaction}>
											Remove
										</Button>
									</>
								)}
								<Button variant="contained" type="button" onClick={onHandleCloseDialog}>
									Cancel
								</Button>
								{isRemoveTransaction && (
									<Button
										color="secondary"
										variant="contained"
										type="button"
										onClick={onHandleConfirmRemoveTransaction}
									>
										Yes (Remove)
									</Button>
								)}
								{!isRemoveTransaction && (
									<Button color="primary" variant="contained" type="button" onClick={onHandleSaveTransaction}>
										Save transaction
									</Button>
								)}
							</>
						}
						// maxWidth={isRemoveTransaction ? 'sm' : 'lg'}
					/>
					{/* Nueva / Editar / Eliminar ArbitrageTransaction Modal */}
					<CustomDialogComponent
						open={openDialogArbitrage}
						onHandleClose={onHandleCloseDialogArbitrageTx}
						titleText={
							setIsEditArbitrageTransaction
								? 'Arbitrage details'
								: isRemoveArbitrageTransaction
								? 'Remove arbitrage'
								: 'Add new arbitrage'
						}
						renderContent={
							isRemoveArbitrageTransaction ? (
								<div>
									<p>Esta acción que estás a punto de hacer no es reversible. ¿Deseas continuar?</p>
								</div>
							) : (
								<ArbitrageTransactionForm
									autosubmit
									disabledInputs={setIsEditArbitrageTransaction ? {} : {}}
									initialValues={arbitrageTransactionValues}
									loading={loadingCreateArbitrageTx || loadingUpdateArbitrageTx}
									onHandleSubmit={onHandleSubmitArbitrageTx}
									transactionOptions={transactionOptions}
								/>
							)
						}
						renderFooter={
							<>
								{setIsEditArbitrageTransaction && (
									<Button color="secondary" variant="contained" type="button" onClick={onHandleRemoveArbitrageTx}>
										Remove
									</Button>
								)}
								<Button variant="contained" type="button" onClick={onHandleCloseDialogArbitrageTx}>
									Cancel
								</Button>
								{isRemoveArbitrageTransaction && (
									<Button
										color="secondary"
										variant="contained"
										type="button"
										onClick={onHandleConfirmRemoveArbitrageTx}
									>
										Yes (Remove)
									</Button>
								)}
								{!isRemoveArbitrageTransaction && (
									<Button color="primary" variant="contained" type="button" onClick={onHandleSaveArbitrageTx}>
										Save arbitrage
									</Button>
								)}
							</>
						}
					/>
				</Grid>
			</Grid>
		</Container>
	)
}

export default OrderDetailContainer
