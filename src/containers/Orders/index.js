import React from 'react'
import { connect, useSelector } from 'react-redux'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { get, isEmpty } from 'lodash'
import { makeStyles, Button, Container, Grid, Typography } from '@material-ui/core'
import { Add as AddIcon, Cached as CachedIcon } from '@material-ui/icons'
import { useHistory } from 'react-router'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import CustomDialogComponent from '../../components/Dialog/CustomDialog'
import Loading from '../../components/Loading'
import OrdersTable from '../../components/Order/OrdersTable'
import OrderForm from '../../components/Order/OrderForm'
import Fare from '../../components/Fare'

// Actions
import { setUserInfoAction } from '../../actions/userActions'

// Hooks
import useLoadUserInfo from '../../hooks/useLoadUserInfo'

// Queries
import { GET_ORDER_LIST } from '../../graphql/queries'
import { ORDER_CREATE, ORDER_DELETE, ORDER_UPDATE } from '../../graphql/mutations'

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: theme.spacing(2),
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
}))

const TEMP_CURRENCIES = [
    'ARS',
    'CLP',
    'COP',
    'EUR',
    'PEN',
    'USD',
    'VES',
    'MXN',
    'BRL'
]

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map(c => ({
    label: c,
    value: c
}))

/**
 * Orders container
 * 
 * @param {any} props
 * @param {Function} props.setUserInfoAction
 */
const OrdersContainer = ({ setUserInfoAction }) => {
	const classes = useStyles()
	const history = useHistory()
    const dataUserInfo = useLoadUserInfo()

	// States
	const [filters, setFilters] = React.useState()
	const [orders, setOrders] = React.useState([])
	const [openDialog, setOpenDialog] = React.useState()
	const [orderValues, setOrderValues] = React.useState({})
	const [isEditOrder, setIsEditOrder] = React.useState()
	const [isRemoveOrder, setIsRemoveOrder] = React.useState()
	const [pathOrderDetails, setPathOrderDetails] = React.useState() // TODO: Usar para Orden asociada y usuario

	// Queries
	const [getOrders, { data: dataOrders, loading: loadingOrders, refetch }] = useLazyQuery(GET_ORDER_LIST)
	const [createOrder, { data: createdOrder, loading: loadingCreateOrder }] = useMutation(ORDER_CREATE)
	const [deleteOrder, { data: deletedOrder, loading: loadingDeleteOrder }] = useMutation(ORDER_DELETE)
	const [updateOrder, { data: updatedOrder, loading: loadingUpdateOrder }] = useMutation(ORDER_UPDATE)

	React.useEffect(() => {
		getOrders({
			variables: { filters },
		})
	}, [])

    React.useEffect(() => {
        if (!isEmpty(dataUserInfo) && !isEmpty(dataUserInfo.userInfo)) {
			// Set User Info
			setUserInfoAction(dataUserInfo.userInfo)
		}
    }, [dataUserInfo, setUserInfoAction])

	// Al resolver las transacciones
	React.useEffect(() => {
		const _items = get(dataOrders, 'ordersList.items')
		if (!isEmpty(_items)) {
			setOrders(_items)
		}
        return () => {
            setOrders([])
        }
	}, [dataOrders])

	// Cuando se ha creado la transacción
	React.useEffect(() => {
		const _created = get(createdOrder, 'orderCreate')
		if (_created && _created._id) {
			// history.push(`/order/${_created._id}`)
			setOrders(prevItems => (!prevItems.length) ? [_created] : [...prevItems, _created])
		}
	}, [createdOrder])

	// Cuando se actualiza una transacción
	React.useEffect(() => {
		const _updated = get(updatedOrder, 'orderUpdate')
		if (_updated) {
			setOrders(prevItems => {
				return prevItems.map(i => {
					if (i._id === _updated._id) {
						i = { ...i, ..._updated }
					}
					return i
				})
			})
		}
	}, [updatedOrder])

	// Cuando se elimina
	React.useEffect(() => {
		const _deleted = get(deletedOrder, 'orderDelete')
		if (_deleted && orderValues) {
			setOrders(prevItems => prevItems.filter(i => i._id !== orderValues._id))
			setOrderValues({})
		}
	}, [deletedOrder, orderValues])

	/**
	 * Recargar tabla de transacciones
	 */
	const onHandleRefresh = () => {
        setOrders([])
        refetch({ variables: { filters } })
    }

	/**
	 * Abrir modal para agregar nueva transacción
	 */
	const onClickNewOrder = () => {
		setOpenDialog(true)
		setOrderValues({
            date: '',
            fromCurrency: '',
			fromAmount: '',
            toCurrency: '',
			toAmount: '',
			baseRate: '',
            contactFullName: '',
            contactEmail: '',
            contactPhone: '',
			toBankInfo: '',
			spreadPercent: '',
			status: 'DRAFT'
		})
	}

	/**
	 * Cerrar modal de agregar campaña
	 */
	const onHandleCloseDialog = () => {
		setOpenDialog(false)
		setIsEditOrder()
		setOrderValues()
		setPathOrderDetails()
		setIsRemoveOrder()
	}

	/**
	 * Al hacer click en una fila de tabla de transacciones
	 *
	 * @param {object} row
	 */
	const onHandleClickRow = (row) => {
		// Editar en modal
		setIsEditOrder(true)

        // Quitar algunos campos
        const { token, transactions, user, userClient, createdAt, updatedAt, ...values } = row

		setOrderValues(values)
		setOpenDialog(true)
		setPathOrderDetails(`/order/${row._id}`)
	}

	/**
	 * Redireccionar a detalles de campaña
	 */
	 const onHandleOrderDetails = () => {
		history.push(pathOrderDetails)
		onHandleCloseDialog()
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	 const onHandleSubmitOrder = (values) => {
        // TODO: Buscar base rate
        
        setOrderValues(values)
     }

	/**
	 * Mostrar modal de confirmación de eliminar campaña
	 */
	 const onHandleRemoveOrder = () => {
		setIsEditOrder(false)
		setIsRemoveOrder(true)
	}

	/**
	 * Confirmación de eliminar el campaña del Backend
	 */
	const onHandleConfirmRemoveOrder = () => {
		// Eliminar del backend
		deleteOrder({
			variables: {
				_id: orderValues._id
			}
		})

		setOpenDialog(false)
		setIsEditOrder()
		setPathOrderDetails()
		setIsRemoveOrder()
	}

	/**
	 * Guardar valores del formulario con GraphQL
	 */
	const onHandleSaveOrder = () => {
		// Nuevo contacto
		if (!isEditOrder && orderValues) {
			// console.log('OrderForm Values:', orderValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			createOrder({
				variables: { input: orderValues }
			})
		}
		// Editar contacto
		else if (isEditOrder && orderValues) {
			// console.log('OrderForm Values:', orderValues)
			
			// Con los valores seteados por AutoSubmit los enviamos a Backend
			updateOrder({
				variables: { input: orderValues }
			})
		}

		onHandleCloseDialog()
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4">
				Orders list
			</Typography>
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
						path: '#',
						label: 'Orders',
					},
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={12} md={12} xs={12}>
					{loadingOrders && <Loading />}

					<p>TODO: Filters</p>
					<Button variant="outlined" onClick={onHandleRefresh}>
						<CachedIcon /> Refresh
					</Button>
					<Button variant="outlined" onClick={onClickNewOrder}>
						<AddIcon /> New
					</Button>

					<OrdersTable onHandleClickRow={onHandleClickRow} data={orders} />

					{/* Nuevo / Editar / Eliminar Order Modal */}
					<CustomDialogComponent
						open={openDialog}
						onHandleClose={onHandleCloseDialog}
						titleText={
							isEditOrder
								? 'Order details'
								: isRemoveOrder
								? 'Remove order'
								: 'Add new order'
						}
						renderContent={
							isRemoveOrder ? (
								<div>
									<p>Esta acción que estás a punto de hacer no es reversible. ¿Deseas continuar?</p>
								</div>
							) : (
								<OrderForm
									autosubmit
                                    currencyOptions={CURRENCIES_ITEMS}
									disabledInputs={
										isEditOrder
											? {}
											: { status: true }
									}
									initialValues={orderValues}
									loading={loadingCreateOrder || loadingUpdateOrder}
									onHandleSubmit={onHandleSubmitOrder}
								/>
							)
						}
						renderFooter={
							<>
                                {orderValues && orderValues.baseRate && (
                                    <p>
                                        <strong>Rate: <Fare amount={orderValues.baseRate} symbol="$" /></strong>
                                    </p>
                                )}
								{isEditOrder && pathOrderDetails && (
									<>
										<Button variant="outlined" type="button" onClick={onHandleOrderDetails}>
											Details
										</Button>
										<Button color="secondary" variant="contained" type="button" onClick={onHandleRemoveOrder}>
											Remove
										</Button>
									</>
								)}
								<Button variant="contained" type="button" onClick={onHandleCloseDialog}>
									Cancel
								</Button>
								{isRemoveOrder && (
									<Button color="secondary" variant="contained" type="button" onClick={onHandleConfirmRemoveOrder}>
										Yes (Remove)
									</Button>
								)}
								{!isRemoveOrder && (
									<Button color="primary" variant="contained" type="button" onClick={onHandleSaveOrder}>
										Save order
									</Button>
								)}
							</>
						}
						// maxWidth={isRemoveTransaction ? 'sm' : 'lg'}
					/>
				</Grid>
			</Grid>
		</Container>
	)
}

export default connect(null, {
    setUserInfoAction
})(OrdersContainer)
