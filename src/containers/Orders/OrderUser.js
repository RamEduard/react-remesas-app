import React from 'react'
import { useHistory, useParams } from 'react-router'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { makeStyles, Container, Grid, Typography, Paper } from '@material-ui/core'
import { get, isEmpty } from 'lodash'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import Loading from '../../components/Loading'
import OrderForm from '../../components/Order/OrderForm'

// Queries
import { GET_ORDER_BY_ID_TOKEN } from '../../graphql/queries'

const TEMP_CURRENCIES = ['ARS', 'CLP', 'COP', 'EUR', 'PEN', 'USD', 'VES', 'MXN', 'BRL']

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map((c) => ({
	label: c,
	value: c,
}))

const useStyles = makeStyles((theme) => ({
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
	paper: {
		padding: theme.spacing(2),
	},
}))

/**
 * OrderUserContainer
 * 
 * TODO: guardar solo los datos permitidos al usuario no logueado - Crear mutation
 */
const OrderUserContainer = () => {
    const classes = useStyles()
    const history = useHistory()
    const { orderId, token } = useParams()

    // State
    const [orderValues, setOrderValues] = React.useState({})

    // Apollo hooks
	const [getOrder, { data: dataOrder, loading: loadingOrder, refetch }] = useLazyQuery(GET_ORDER_BY_ID_TOKEN)

    React.useEffect(() => {
		if (orderId && orderId !== '' && token && token !== '') {
			getOrder({ variables: { _id: orderId, token } })
		}
	}, [orderId, token])

    // Al obtener los datos de la orden
	React.useEffect(() => {
		const _order = get(dataOrder, 'orderByIdAndToken')
		if (!isEmpty(_order)) {
			const { createdAt, updatedAt, ...orderVals } = _order
			setOrderValues(orderVals)
		}
		return () => {
			setOrderValues({})
		}
	}, [dataOrder])

    return (
        <Container maxWidth="xl">
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
						path: '/orders',
						label: 'Orders',
					},
					{
						path: '#',
						label: 'Order details',
					},
				]}
			/>
			<Grid container justify="center" spacing={2} className={classes.margin}>
				<Grid item lg={3} md={4} xs={12}>
					{loadingOrder && <Loading />}
					<Paper className={classes.paper}>
						<Typography component="h2" variant="h5">
							Order details
						</Typography>
						<OrderForm
							initialValues={orderValues}
                            disabledInputs={{
                                fromCurrency: true,
                                toCurrency: true,
                                toAmount: true,
                                status: true,
                                baseRate: true
                            }}
							currencyOptions={CURRENCIES_ITEMS}
							// onHandleSubmit={onHandleSubmitOrder}
							onHandleCancel={() => history.goBack()}
						/>
					</Paper>
				</Grid>
            </Grid>
        </Container>
    )
}

export default OrderUserContainer