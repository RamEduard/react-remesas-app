import React from 'react'
import { connect, useSelector } from 'react-redux'
import { useLazyQuery } from '@apollo/react-hooks'
import { get, isEmpty } from 'lodash'
import { makeStyles, Button, Container, Grid, Typography, Paper, Stepper, Step, StepLabel } from '@material-ui/core'

// Internal components
import Breadcrumb from '../components/Breadcrumb'
import NewOrderForm from '../components/Order/NewOrderForm'
import ContactDataForm from '../components/Order/ContactDataForm'

// Actions
import { setUserInfoAction } from '../actions/userActions'

// Queries
import { GET_USER_INFO } from '../graphql/queries'

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: theme.spacing(2),
	},
	stepper: {
		padding: theme.spacing(3, 0, 5),
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		paddingTop: theme.spacing(2),
	},
}))

const STEPS = [
	'Create Order',
	'Contact info'
]

/**
 * Dashboard container
 *
 * @param {any} props
 * @param {Function} props.setUserInfoAction
 */
const HomeContainer = ({ setUserInfoAction }) => {
	const classes = useStyles()
	const { currentUser, token } = useSelector((state) => state.user)

	// States
	const [activeStep, setActiveStep] = React.useState(0)
	const [orderData, setOrderData] = React.useState()
	const [contactData, setContactData] = React.useState()

	// Queries
	const [getUserInfo, { data: dataUserInfo }] = useLazyQuery(GET_USER_INFO)

	React.useEffect(() => {
		if (currentUser === null && !isEmpty(token)) {
			// Get Current User
			getUserInfo()
		}
	}, [currentUser, token, getUserInfo])

	React.useEffect(() => {
		if (!isEmpty(dataUserInfo) && !isEmpty(dataUserInfo.userInfo)) {
			// Set User Info
			setUserInfoAction(dataUserInfo.userInfo)
		}
	}, [dataUserInfo, setUserInfoAction])

	/**
	 * Handle back Stepper
	 */
	const handleBack = () => {
		setActiveStep(activeStep - 1)
	}

	/**
	 * Handle next Stepper
	 */
	const handleNext = () => {
		// Place Order
		if (activeStep === 2) {
			// Create order
			
			// window.location.replace('/orders')
		}

		setActiveStep(activeStep + 1)
	}

	const onSubmitNewOrder = (values) => {
		console.log(values)
		setOrderData(values)
	}

	const onSubmitContactData = (values) => {
		console.log(values)
		setContactData(values)
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4">
				Remesas App - Beta
			</Typography>
			<Breadcrumb
				links={[
					{
						path: '#',
						label: 'Home',
					},
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={9} md={8} xs={12}>
					<Paper className={classes.paper}>
						<Stepper activeStep={activeStep} className={classes.stepper}>
							{STEPS.map((step) => (
								<Step key={step}>
									<StepLabel>{step}</StepLabel>
								</Step>
							))}
						</Stepper>
						<React.Fragment>
							{activeStep === STEPS.length ? (
								<React.Fragment>
									<Typography variant="h5" gutterBottom>
										Thank you for your order.
									</Typography>
									<Typography variant="subtitle1">
										We have received your order, and will send you
										an update when your order has delivered.
									</Typography>
								</React.Fragment>
							) : (
								<React.Fragment>
									{/* Step 1 - Create Order */}
									{activeStep === 0 && (
										<NewOrderForm
											currencyOptions={[
												{
													label: 'ARS',
													value: 'ARS',
												},
												{
													label: 'VES',
													value: 'VES',
												},
											]}
											initialValues={orderData}
											onSubmit={onSubmitNewOrder}
											autoSubmit
										/>
									)}
									{activeStep === 1 && (
										<ContactDataForm
											initialValues={contactData}
											onSubmit={onSubmitContactData}
										/>
									)}
									<div className={classes.buttons}>
										{activeStep !== 0 && (
											<Button onClick={handleBack} className={classes.button}>
												Back
											</Button>
										)}
										<Button
											variant="contained"
											color="primary"
											onClick={handleNext}
											disabled={
												((!orderData ||
													orderData === null ||
													orderData === {}) && activeStep === 0) ||
												((!contactData ||
												contactData === null ||
												contactData === {}) && activeStep === 1)
											}
											className={classes.button}
										>
											{activeStep === STEPS.length - 1 ? 'Place order' : 'Next'}
										</Button>
									</div>
								</React.Fragment>
							)}
						</React.Fragment>
					</Paper>
				</Grid>
			</Grid>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={6}>
					
				</Grid>
			</Grid>
		</Container>
	)
}

export default connect(null, {
	setUserInfoAction,
})(HomeContainer)
