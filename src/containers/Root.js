import React from 'react'
import { Provider } from 'react-redux'
import { ApolloProvider } from '@apollo/client'
import { ApolloProvider as ApolloHooksProvider } from '@apollo/react-hooks'

import Routes from '../routes'
import store from '../redux/store'
import { client } from '../apollo/client'

const Root = () => (
	<ApolloProvider client={client}>
		<ApolloHooksProvider client={client}>
			<Provider store={store}>
				<Routes />
			</Provider>
		</ApolloHooksProvider>
	</ApolloProvider>
)

export default Root