import React from 'react'
import { connect, useSelector } from 'react-redux'
import { useLazyQuery } from '@apollo/react-hooks'
import { get, isEmpty } from 'lodash'

// Internal components
import Breadcrumb from '../components/Breadcrumb'

// Actions
import { setUserInfoAction } from '../actions/userActions'

// Queries
import { GET_DASHBOARD, GET_USER_INFO, GET_RATES_BY_CURRENCY } from '../graphql/queries'
import { makeStyles, Container, Grid, Typography, Button } from '@material-ui/core'
import RatesTableComponent from '../components/Table/RatesTable'
import RateCard from '../components/Card/RateCard'

const useStyles = makeStyles((theme) => ({
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		paddingTop: theme.spacing(2),
	},
}))

const VALID_CURRENCIES = {
	ARS: 'Peso Argentino',
	CLP: 'Peso Chileno',
	COP: 'Peso Colombiano',
	EUR: 'Euro',
	MXN: 'Peso Mexicano',
	USD: 'Dolar',
	VES: 'Bolivar Venezolano',
}

const VALID_CURRENCIES_KEYS = Object.keys(VALID_CURRENCIES)

const VALID_CURRENCIES_QTY = VALID_CURRENCIES_KEYS.length

/**
 * Home container
 *
 * @param {any} props
 * @param {Function} props.setUserInfoAction
 */
const DashboardContainer = ({ setUserInfoAction }) => {
	const classes = useStyles()
	const { currentUser, token } = useSelector((state) => state.user)

	// Queries
	const [getDashboard, { data: dataDashboard, loading: loadingDashboard }] = useLazyQuery(GET_DASHBOARD)
	const [getUserInfo, { data: dataUserInfo }] = useLazyQuery(GET_USER_INFO)
	const [getRatesByCurrency, { data: dataRatesByCurrency, loading }] = useLazyQuery(GET_RATES_BY_CURRENCY)

	// States
	const [currencyIndex, setCurrencyIndex] = React.useState(0)
	const [ratesDataTable, setRatesDataTable] = React.useState([])
	const [btcRates, setBtcRates] = React.useState()

	React.useEffect(() => {
		if (currentUser === null && !isEmpty(token)) {
			// Get Current User
			getUserInfo()
		}
	}, [currentUser, token, getUserInfo])

	React.useEffect(() => {
		if (!isEmpty(dataUserInfo) && !isEmpty(dataUserInfo.userInfo)) {
			// Set User Info
			setUserInfoAction(dataUserInfo.userInfo)
		}
	}, [dataUserInfo, setUserInfoAction])

	React.useEffect(() => {
		if (currentUser !== null && !isEmpty(token)) {
			// Obtener la primera
			getRatesByCurrency({
				variables: { currencyCode: VALID_CURRENCIES_KEYS[currencyIndex] },
			})

			// Get Dashboard
			getDashboard()
		}
	}, [currentUser, token, currencyIndex, getRatesByCurrency, getDashboard])

	const currentRates = ratesDataTable
	const currentCurrencyIndex = currencyIndex

	// Agregar rates summary to ratesDataTable
	React.useEffect(() => {
		if (get(dataRatesByCurrency, 'ratesByCurrency.avg.rates.last')) {
			const rate = {
				currency: get(dataRatesByCurrency, 'ratesByCurrency.avg.currency'),
				avg: get(dataRatesByCurrency, 'ratesByCurrency.avg.rates.last'),
				buy: get(dataRatesByCurrency, 'ratesByCurrency.buy.first'),
				sell: get(dataRatesByCurrency, 'ratesByCurrency.sell.first'),
				spread: Math.round(Number(get(dataRatesByCurrency, 'ratesByCurrency.spread.first')) * 10000) / 100, // % percent
			}

			if (!currentRates.find((r) => rate.currency === r.currency)) setRatesDataTable([...currentRates, rate])

			if (VALID_CURRENCIES_QTY - 1 > currentCurrencyIndex) {
				getRatesByCurrency({
					variables: { currencyCode: VALID_CURRENCIES_KEYS[currentCurrencyIndex + 1] },
				})
				setCurrencyIndex(currentCurrencyIndex + 1)
			}
		}
	}, [dataRatesByCurrency, getRatesByCurrency])

	React.useEffect(() => {
		if (get(dataDashboard, 'dashboard')) {
			// btcRates
			setBtcRates(get(dataDashboard, 'dashboard.btcRates'))
		}
	}, [dataDashboard])

	// Listeners

	/**
	 * Refresh rates
	 */
	const onReloadRates = () => {
		setCurrencyIndex(0)

		// Obtener la primera
		getRatesByCurrency({
			variables: { currencyCode: VALID_CURRENCIES_KEYS[currencyIndex] },
		})
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4">
				Remesas App - Beta
			</Typography>
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
                        path: '#',
                        label: 'Dashboard',
                    },
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				{/* Rates Cards */}
				<Grid container lg={12}>
					{loadingDashboard && <p>Loading rates for user...</p>}
					{btcRates && btcRates.map((r, i) => <RateCard key={i} label={r.label} pair={r.pair} rate={r.rate} />)}
				</Grid>
				<Grid item lg={9}>
					{loading && <p>Loading rates for {`${VALID_CURRENCIES_KEYS[currencyIndex]}`} currency...</p>}
					<Button variant="contained" onClick={onReloadRates}>
						Reload rates
					</Button>
					<RatesTableComponent data={ratesDataTable} />
				</Grid>
			</Grid>
		</Container>
	)
}

export default connect(null, {
	setUserInfoAction,
})(DashboardContainer)
