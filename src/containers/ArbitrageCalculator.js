import React from 'react'
import { makeStyles, Container, Typography } from '@material-ui/core'
import { useLazyQuery } from '@apollo/react-hooks'
import get from 'lodash/get'
import moment from 'moment'

// Internal components
import ArbitrageCalculator from '../components/Arbitrage/ArbitrageCalculator'
import Breadcrumb from '../components/Breadcrumb'

// GraphQL
import { GET_HOURLY_RATES } from '../graphql/queries'

const TEMP_CURRENCIES = [
    'ARS',
    'CLP',
    'COP',
    'EUR',
    'PEN',
    'USD',
    'VES',
    'MXN',
    'BRL'
]

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map(c => ({
    label: c,
    value: c
}))

const useStyles = makeStyles(theme => ({
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(3),
		marginBottom: theme.spacing(3),
		paddingTop: theme.spacing(2),
    },
    formControl: {
        width: '100%'
    }
}))

const ArbitrageCalculatorContainer = () => {
    const classes = useStyles()
    const breadcrumbLinks = [
        {path: '/', label: 'Home'},
        {path: '#', label: 'Arbitrage Calculator'}
    ]

    const [dataBtcAvg, setDataBtcAvg] = React.useState()

    // Queries
    const [getHourlyRates, { data }] = useLazyQuery(GET_HOURLY_RATES)

    const handleOnSubmitCalculator = values => {
        console.log(values)
    }

    React.useEffect(() => {
        const dateHour = moment.utc().subtract(1, 'hour').set({ minute: 0, second: 0, millisecond: 0 }).toDate()

        console.log(dateHour.getTime())

        getHourlyRates({
            variables: {
                filters: {
                    date: dateHour.getTime()
                }
            }
        })
    }, [getHourlyRates])

    React.useEffect(() => {
        if (get(data, 'hourlyRatesList.items')) {
            // Set data
            setDataBtcAvg(get(data, 'hourlyRatesList.items'))
        }
    }, [data])

    return (
        <Container maxWidth="lg">
            <Typography component="h1" variant="h4">
                Arbitrage Calculator
            </Typography>
            <Breadcrumb links={breadcrumbLinks} />
            <br />
            <br />
            <ArbitrageCalculator 
                classes={classes} 
                currencyItems={CURRENCIES_ITEMS}
                dataBtcAvg={dataBtcAvg}
                onSubmit={handleOnSubmitCalculator}
            />
        </Container>
    )
}

export default ArbitrageCalculatorContainer
