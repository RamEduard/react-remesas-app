import React from 'react'
import { connect, useSelector } from 'react-redux'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { get, isEmpty } from 'lodash'
import { makeStyles, Button, Container, Grid, Typography, Paper, Stepper, Step, StepLabel } from '@material-ui/core'
import { Add as AddIcon, Cached as CachedIcon } from '@material-ui/icons'
import { useHistory } from 'react-router'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import CustomDialogComponent from '../../components/Dialog/CustomDialog'
import Loading from '../../components/Loading'
import TransactionsTable from '../../components/Transaction/TransactionsTable'
import TransactionForm from '../../components/Transaction/TransactionForm'

// Queries
import { GET_TRANSACTION_LIST } from '../../graphql/queries'
import { TRANSACTION_CREATE, TRANSACTION_DELETE, TRANSACTION_UPDATE } from '../../graphql/mutations'

const useStyles = makeStyles((theme) => ({
	paper: {
		padding: theme.spacing(2),
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	button: {
		marginTop: theme.spacing(3),
		marginLeft: theme.spacing(1),
	},
	actionButton: {
		marginTop: theme.spacing(2),
		textAlign: 'right',
	},
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
}))

/**
 * Transactions container
 */
const TransactionsContainer = () => {
	const classes = useStyles()
	const history = useHistory()

	// States
	const [filters, setFilters] = React.useState()
	const [transactions, setTransactions] = React.useState([])
	const [openDialog, setOpenDialog] = React.useState()
	const [transactionValues, setTransactionValues] = React.useState({})
	const [isEditTransaction, setIsEditTransaction] = React.useState()
	const [isRemoveTransaction, setIsRemoveTransaction] = React.useState()
	const [pathTransactionDetails, setPathTransactionDetails] = React.useState() // TODO: Usar para Orden asociada y usuario

	// Queries
	const [getTransactions, { data: dataTransactions, loading: loadingTxs, refetch }] = useLazyQuery(GET_TRANSACTION_LIST)
	const [createTransaction, { data: createdTransaction, loading: loadingCreateTx }] = useMutation(TRANSACTION_CREATE)
	const [deleteTransaction, { data: deletedTransaction, loading: loadingDeleteTx }] = useMutation(TRANSACTION_DELETE)
	const [updateTransaction, { data: updatedTransaction, loading: loadingUpdateTx }] = useMutation(TRANSACTION_UPDATE)

	React.useEffect(() => {
		getTransactions({
			variables: { filters },
		})
	}, [])

	// Al resolver las transacciones
	React.useEffect(() => {
		const _items = get(dataTransactions, 'transactionsList.items')
		if (!isEmpty(_items)) {
			setTransactions(_items)
		}
	}, [dataTransactions])

	// Cuando se ha creado la transacción
	React.useEffect(() => {
		const _created = get(createdTransaction, 'transactionCreate')
		if (_created && _created._id) {
			// history.push(`/transaction/${_created._id}`)
			setTransactions(prevItems => (!prevItems.length) ? [_created] : [...prevItems, _created])
		}
	}, [createdTransaction])

	// Cuando se actualiza una transacción
	React.useEffect(() => {
		const _updated = get(updatedTransaction, 'transactionUpdate')
		if (_updated) {
			setTransactions(prevItems => {
				return prevItems.map(i => {
					if (i._id === _updated._id) {
						i = _updated
					}
					return i
				})
			})
		}
	}, [updatedTransaction])

	// Cuando se elimina
	React.useEffect(() => {
		const _deleted = get(deletedTransaction, 'transactionDelete')
		if (_deleted && transactionValues) {
			setTransactions(prevItems => prevItems.filter(i => i._id !== transactionValues._id))
			setTransactionValues()
		}
	}, [deletedTransaction, transactionValues])

	/**
	 * Recargar tabla de transacciones
	 */
	const onHandleRefresh = () => refetch({ variables: { filters } })

	/**
	 * Abrir modal para agregar nueva transacción
	 */
	const onClickNewTransaction = () => {
		setOpenDialog(true)
		setTransactionValues({
			type: '',
			currency: '',
			currencyLabel: '',
			amountCurrency: '',
			amountBtc: '',
			rate: '',
			paymentMethod: '',
			paymentMethodDescription: ''
		})
	}

	/**
	 * Cerrar modal de agregar campaña
	 */
	const onHandleCloseDialog = () => {
		setOpenDialog(false)
		setIsEditTransaction()
		setTransactionValues()
		setPathTransactionDetails()
		setIsRemoveTransaction()
	}

	/**
	 * Al hacer click en una fila de tabla de transacciones
	 *
	 * @param {object} row
	 */
	const onHandleClickRow = (row) => {
		// Editar en modal
		setIsEditTransaction(true)

		// Quitar algunos campos
        const { user, createdAt, updatedAt, ...values } = row

		setTransactionValues(values)
		setOpenDialog(true)
		setPathTransactionDetails(`/transaction/${row._id}`)
	}

	/**
	 * Redireccionar a detalles de campaña
	 */
	 const onHandleTransactionDetails = () => {
		history.push(pathTransactionDetails)
		onHandleCloseDialog()
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	 const onHandleSubmitTransaction = (values) => (setTransactionValues(values))

	/**
	 * Mostrar modal de confirmación de eliminar campaña
	 */
	 const onHandleRemoveTransaction = () => {
		setIsEditTransaction(false)
		setIsRemoveTransaction(true)
	}

	/**
	 * Confirmación de eliminar el campaña del Backend
	 */
	const onHandleConfirmRemoveTransaction = () => {
		// Eliminar del backend
		deleteTransaction({
			variables: {
				_id: transactionValues._id
			}
		})

		setOpenDialog(false)
		setIsEditTransaction()
		setPathTransactionDetails()
		setIsRemoveTransaction()
	}

	/**
	 * Guardar valores del formulario con GraphQL
	 */
	const onHandleSaveTransaction = () => {
		// Nuevo contacto
		if (!isEditTransaction && transactionValues) {
			// console.log('TransactionForm Values:', transactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			createTransaction({
				variables: { input: transactionValues }
			})
		}
		// Editar contacto
		else if (isEditTransaction && transactionValues) {
			// console.log('TransactionForm Values:', transactionValues)
			
			// Con los valores seteados por AutoSubmit los enviamos a Backend
			updateTransaction({
				variables: { input: transactionValues }
			})
		}

		onHandleCloseDialog()
	}

	return (
		<Container maxWidth="lg">
			<Typography component="h1" variant="h4">
				Transactions list
			</Typography>
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
						path: '#',
						label: 'Transactions',
					},
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={12} md={12} xs={12}>
					{loadingTxs && <Loading />}

					<p>TODO: Filters</p>
					<Button variant="outlined" onClick={onHandleRefresh}>
						<CachedIcon /> Refresh
					</Button>
					<Button variant="outlined" onClick={onClickNewTransaction}>
						<AddIcon /> New
					</Button>

					<TransactionsTable onHandleClickRow={onHandleClickRow} data={transactions} />

					{/* Nuevo / Editar / Eliminar Transaction Modal */}
					<CustomDialogComponent
						open={openDialog}
						onHandleClose={onHandleCloseDialog}
						titleText={
							isEditTransaction
								? 'Transaction details'
								: isRemoveTransaction
								? 'Remove transaction'
								: 'Add new transaction'
						}
						renderContent={
							isRemoveTransaction ? (
								<div>
									<p>Esta acción que estás a punto de hacer no es reversible. ¿Deseas continuar?</p>
								</div>
							) : (
								<TransactionForm
									autosubmit
									disabledInputs={
										isEditTransaction
											? {
													nombre: true,
													descripcion: true,
													comienzo: true,
													contact_flow_id: true,
													estado: true,
													fin: true,
													reintentos: true,
													espera_entre_reintentos: true,
											  }
											: {}
									}
									initialValues={transactionValues}
									loading={loadingCreateTx || loadingUpdateTx}
									onHandleSubmit={onHandleSubmitTransaction}
								/>
							)
						}
						renderFooter={
							<>
								{isEditTransaction && pathTransactionDetails && (
									<>
										<Button variant="outlined" type="button" onClick={onHandleTransactionDetails}>
											More details
										</Button>
										<Button color="secondary" variant="contained" type="button" onClick={onHandleRemoveTransaction}>
											Remove
										</Button>
									</>
								)}
								<Button variant="contained" type="button" onClick={onHandleCloseDialog}>
									Cancel
								</Button>
								{isRemoveTransaction && (
									<Button color="secondary" variant="contained" type="button" onClick={onHandleConfirmRemoveTransaction}>
										Yes (Remove)
									</Button>
								)}
								{!isRemoveTransaction && (
									<Button color="primary" variant="contained" type="button" onClick={onHandleSaveTransaction}>
										Save transaction
									</Button>
								)}
							</>
						}
						// maxWidth={isRemoveTransaction ? 'sm' : 'lg'}
					/>
				</Grid>
			</Grid>
		</Container>
	)
}

export default TransactionsContainer
