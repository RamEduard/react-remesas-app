import React from 'react'
import { useHistory, useParams } from 'react-router'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { makeStyles, Button, Container, Grid, Typography, Paper } from '@material-ui/core'
import { Add as AddIcon, Cached as CachedIcon } from '@material-ui/icons'
import { get, isEmpty } from 'lodash'

// Internal components
import Breadcrumb from '../../components/Breadcrumb'
import CustomDialogComponent from '../../components/Dialog/CustomDialog'
import BankTransferForm from '../../components/BankTransfer/BankTransferForm'
import BankTransfersTable from '../../components/BankTransfer/BankTransfersTable'
// import Fare from '../../components/Fare'
import Loading from '../../components/Loading'
import TransactionForm from '../../components/Transaction/TransactionForm'

// Queries
import { GET_TRANSACTION } from '../../graphql/queries'
import {
    BANK_TRANSFER_CREATE,
    BANK_TRANSFER_DELETE,
    BANK_TRANSFER_UPDATE,
	TRANSACTION_UPDATE,
} from '../../graphql/mutations'

const TEMP_CURRENCIES = ['ARS', 'CLP', 'COP', 'EUR', 'PEN', 'USD', 'VES', 'MXN', 'BRL']

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map((c) => ({
	label: c,
	value: c,
}))

const useStyles = makeStyles((theme) => ({
	margin: {
		marginTop: theme.spacing(2),
		marginBottom: theme.spacing(2),
	},
	paper: {
		padding: theme.spacing(2),
	},
}))

const TransactionDetailsContainer = () => {
	const classes = useStyles()
	const history = useHistory()
	const { transactionId } = useParams()

	// States
	const [openDialog, setOpenDialog] = React.useState()

	// Order states
	const [transactionValues, setTransactionValues] = React.useState({})
	// BankTransfers states
	const [bankTransfers, setBankTransfers] = React.useState([])
	const [bankTransferValues, setBankTransferValues] = React.useState({})
	const [isEditBankTransfer, setIsEditBankTransfer] = React.useState()
	const [isRemoveBankTransfer, setIsRemoveBankTransfer] = React.useState()
	const [pathBankTransferDetails, setPathBankTransferDetails] = React.useState()

	// Apollo hooks
	const [getTransaction, { data: dataTx, loading: loadingTx, refetch }] = useLazyQuery(GET_TRANSACTION)
	const [updateTransaction, { data: updatedTransaction, loading: loadingUpdateTx }] = useMutation(TRANSACTION_UPDATE)
    // Bank transfers
    const [createBankTransfer, { data: createdBankTransfer, loading: loadingCreateBankTransfer }] = useMutation(BANK_TRANSFER_CREATE)
	const [deleteBankTransfer, { data: deletedBankTransfer, loading: loadingDeleteBankTransfer }] = useMutation(BANK_TRANSFER_DELETE)
	const [updateBankTransfer, { data: updatedBankTransfer, loading: loadingUpdateBankTransfer }] = useMutation(BANK_TRANSFER_UPDATE)

	React.useEffect(() => {
		if (transactionId && transactionId !== '') {
			getTransaction({ variables: { _id: transactionId } })
		}
	}, [transactionId])

	// Al obtener los datos de la orden
	React.useEffect(() => {
		const _transaction = get(dataTx, 'transactionGet')
		const _bankTransfers = get(dataTx, 'transactionGet.bankTransfers') || []
		if (!isEmpty(_transaction)) {
			setBankTransfers(_bankTransfers)
			const { bankTransfers, user, createdAt, updatedAt, ...transactionVals } = _transaction
			setTransactionValues(transactionVals)
		}
		return () => {
			setTransactionValues({})
			setBankTransfers([])
		}
	}, [dataTx])

	// Cuando se ha creado la transferencia bancaria
	React.useEffect(() => {
		const _created = get(createdBankTransfer, 'bankTransferCreate')
		if (_created && _created._id) {
			// history.push(`/transaction/${_created._id}`)
			setBankTransfers((prevItems) => (!prevItems.length ? [_created] : [_created, ...prevItems]))
		}
	}, [createdBankTransfer])

	// Cuando se actualiza una transferencia bancaria
	React.useEffect(() => {
		const _updated = get(updatedBankTransfer, 'bankTransferUpdate')
		if (_updated) {
			setBankTransfers((prevItems) => {
				return prevItems.map((i) => {
					if (i._id === _updated._id) {
						i = { ...i, ..._updated }
					}
					return i
				})
			})
		}
	}, [updatedBankTransfer])

	// Cuando se elimina transferencia bancaria
	React.useEffect(() => {
		const _deleted = get(deletedBankTransfer, 'bankTransferDelete')
		if (_deleted && bankTransferValues) {
			setBankTransfers((prevItems) => prevItems.filter((i) => i._id !== bankTransferValues._id))
			setBankTransferValues()
		}
	}, [deletedBankTransfer, bankTransferValues])

	/**
	 * Recargar tabla de transacciones
	 */
	const onHandleRefresh = () => {
		refetch({ variables: { _id: transactionId } })
	}

	/**
	 * Cerrar modal de agregar transferencia bancaria
	 */
	const onHandleCloseDialog = () => {
		setOpenDialog(false)
		setIsEditBankTransfer()
		setBankTransferValues()
		setPathBankTransferDetails()
		setIsRemoveBankTransfer()
	}

	/**
	 * Al hacer click en una fila de tabla de transferencias bancarias
	 *
	 * @param {object} row
	 */
	const onHandleClickRow = (row) => {
		// Editar en modal
		setIsEditBankTransfer(true)

		// Quitar algunos campos
		const { user, createdAt, updatedAt, ...values } = row

		setBankTransferValues(values)
		setOpenDialog(true)
		setPathBankTransferDetails(`/bank-transfer/${row._id}`)
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	const onHandleSubmitTransaction = (values) => {
		// console.log('OrderForm Values:', values)

		// Con los valores seteados por AutoSubmit los enviamos a Backend
		updateTransaction({
			variables: { input: values },
		})
	}

	/**
	 * Abrir modal para agregar nueva transferencia bancaria
	 */
	const onClickNewBankTransfer = () => {
		setOpenDialog(true)
		setBankTransferValues({
            amount: '',
            bankDescription: '',
            currency: '',
            date: '',
            imageUrl: '',
            status: 'DRAFT',
            transactionId
		})
	}

	/**
	 * Redireccionar a detalles de campaña
	 */
	const onHandleBankTransferDetails = () => {
		history.push(pathBankTransferDetails)
		onHandleCloseDialog()
	}

	/**
	 * Cuando se envian los datos del formulario
	 * @param {object} {values} Valores del formulario
	 */
	const onHandleSubmitBankTransfer = (values) => setBankTransferValues(values)

	/**
	 * Mostrar modal de confirmación de eliminar campaña
	 */
	const onHandleRemoveBankTransfer = () => {
		setIsEditBankTransfer(false)
		setIsRemoveBankTransfer(true)
	}

	/**
	 * Confirmación de eliminar el transferencia bancaria del Backend
	 */
	const onHandleConfirmRemoveBankTransfer = () => {
		// Eliminar del backend
		deleteBankTransfer({
			variables: {
				_id: bankTransferValues._id,
			},
		})

		setOpenDialog(false)
		setIsEditBankTransfer()
		setPathBankTransferDetails()
		setIsRemoveBankTransfer()
	}

	/**
	 * Guardar valores del formulario con GraphQL
	 */
	const onHandleSaveBankTransfer = () => {
		// Nuevo contacto
		if (!isEditBankTransfer && bankTransferValues) {
			// console.log('TransactionForm Values:', transactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			createBankTransfer({
				variables: { input: bankTransferValues },
			})
		}
		// Editar contacto
		else if (isEditBankTransfer && bankTransferValues) {
			// console.log('TransactionForm Values:', transactionValues)

			// Con los valores seteados por AutoSubmit los enviamos a Backend
			updateBankTransfer({
				variables: { input: bankTransferValues },
			})
		}

		onHandleCloseDialog()
	}

	return (
		<Container maxWidth="xl">
			<Breadcrumb
				links={[
					{
						path: '/',
						label: 'Home',
					},
					{
						path: '/transactions',
						label: 'Transactions',
					},
					{
						path: '#',
						label: 'Transaction details',
					},
				]}
			/>
			<Grid container spacing={2} className={classes.margin}>
				<Grid item lg={3} md={4} xs={12}>
					{loadingTx && <Loading />}
					<Paper className={classes.paper}>
						<Typography component="h2" variant="h5">
							Transaction details
						</Typography>
						<TransactionForm
							initialValues={transactionValues}
							currencyOptions={CURRENCIES_ITEMS}
							onHandleSubmit={onHandleSubmitTransaction}
							onHandleCancel={() => history.goBack()}
						/>
					</Paper>
				</Grid>
				<Grid item lg={9} md={8} xs={12}>
					<Typography component="h2" variant="h5">
						Bank transfers related to
					</Typography>
					<Button variant="outlined" onClick={onHandleRefresh}>
						<CachedIcon /> Refresh
					</Button>
					<Button variant="outlined" onClick={onClickNewBankTransfer}>
						<AddIcon /> New
					</Button>
					{/* BankTransfers table */}
					<BankTransfersTable onHandleClickRow={onHandleClickRow} data={bankTransfers} />

					{/* Nuevo / Editar / Eliminar Transaction Modal */}
					<CustomDialogComponent
						open={openDialog}
						onHandleClose={onHandleCloseDialog}
						titleText={
							isEditBankTransfer
								? 'Bank transfer details'
								: isRemoveBankTransfer
								? 'Remove bank transfer'
								: 'Add new bank transfer'
						}
						renderContent={
							isRemoveBankTransfer ? (
								<div>
									<p>Esta acción que estás a punto de hacer no es reversible. ¿Deseas continuar?</p>
								</div>
							) : (
								<BankTransferForm
									autosubmit
                                    currencyOptions={CURRENCIES_ITEMS}
									disabledInputs={isEditBankTransfer ? {} : { status: true }}
									initialValues={bankTransferValues}
									loading={loadingCreateBankTransfer || loadingUpdateBankTransfer}
									onHandleSubmit={onHandleSubmitBankTransfer}
								/>
							)
						}
						renderFooter={
							<>
								{isEditBankTransfer && pathBankTransferDetails && (
									<>
										<Button variant="outlined" type="button" onClick={onHandleBankTransferDetails}>
											More details
										</Button>
										<Button color="secondary" variant="contained" type="button" onClick={onHandleRemoveBankTransfer}>
											Remove
										</Button>
									</>
								)}
								<Button variant="contained" type="button" onClick={onHandleCloseDialog}>
									Cancel
								</Button>
								{isRemoveBankTransfer && (
									<Button
										color="secondary"
										variant="contained"
										type="button"
										onClick={onHandleConfirmRemoveBankTransfer}
									>
										Yes (Remove)
									</Button>
								)}
								{!isRemoveBankTransfer && (
									<Button color="primary" variant="contained" type="button" onClick={onHandleSaveBankTransfer}>
										Save bank transfer
									</Button>
								)}
							</>
						}
						// maxWidth={isRemoveTransaction ? 'sm' : 'lg'}
					/>
				</Grid>
			</Grid>
		</Container>
	)
}

export default TransactionDetailsContainer
