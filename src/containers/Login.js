import React from 'react'
import {
	Avatar,
	Container,
	Grid,
	Link as MaterialLink,
	Typography,
	makeStyles,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Alert from '@material-ui/lab/Alert'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { connect, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { get, isEmpty } from 'lodash'

// Actions
import { loggedInAction } from '../actions/userActions'

// Internal components
import LoginForm from '../components/User/LoginForm'

// GQL
import { SIGN_IN } from '../graphql/mutations'
import { GET_USER_INFO } from '../graphql/queries'

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
}))

const LoginContainer = ({ loggedInAction }) => {
	const classes = useStyles()
	const history = useHistory()
	const { currentUser, token } = useSelector(state => state.user)

	const [loggedIn, setLoggedIn] = React.useState()

	// Mutation y queries
	const [getUserInfo, { data: dataUserInfo, loading: loadingUserInfo }] = useLazyQuery(GET_USER_INFO)
	const [signIn, { data, error: errorLogin, loading: loadingLogin }] = useMutation(SIGN_IN)

	const onHandleSubmit = async (values, actions) => {
		try {
			await signIn({ variables: { email: values.email, password: values.password } })
			actions.resetForm()
		} catch (e) {
			console.log('Error logged in')
		}
		actions.setSubmitting(false)
	}

	React.useEffect(() => {
		if (data && !errorLogin) {
			loggedInAction(data.signin.token).then(() => {
				setLoggedIn(true)
			})
		}
	}, [data, errorLogin, loggedInAction])

	React.useEffect(() => {
		if (get(currentUser, 'email') || !isEmpty(token)) {
			// Get Current User
            getUserInfo()
		}
	}, [currentUser, token, getUserInfo])

	React.useEffect(() => {
        if (!isEmpty(dataUserInfo) && !isEmpty(dataUserInfo.userInfo)) {
            setLoggedIn(true)
        }
    }, [dataUserInfo])

	React.useEffect(() => {
		if (loggedIn && ((get(currentUser, 'email') || !isEmpty(token)))) {
			setTimeout(() => {
				history.replace('/')
			}, 1000)
		}
	}, [currentUser, history, loggedIn, token])

	return (
		<React.Fragment>
			<Container maxWidth="xs">
				<div className={classes.paper}>
					<Avatar className={classes.avatar}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					{errorLogin && <Alert severity="error">Bad credentials.</Alert>}
					{!errorLogin && currentUser && loggedIn && <Alert severity="success">Login successful. Redirecting...</Alert>}
					<LoginForm loading={loadingLogin || loadingUserInfo} onSubmit={onHandleSubmit} />
					<Grid container>
						<Grid item xs></Grid>
						<Grid item>
							<Link to="/register" component={MaterialLink}>
								{"Don't have an account? Register"}
							</Link>
						</Grid>
					</Grid>
				</div>
			</Container>
		</React.Fragment>
	)
}

export default connect(null, { loggedInAction })(LoginContainer)
