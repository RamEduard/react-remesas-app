import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import moment from 'moment'

// Internal components
import AutoSubmit from '../Form/AutoSubmit'
import GenericItemsSelector from '../Form/GenericItemsSelector'
import InputField from '../Form/InputField'
import InputDateTimePicker from '../Form/InputDateTimePicker'
import Fare from '../Fare'
import { Grid } from '@material-ui/core'

const VALIDATION_SCHEMA = Yup.object({
    fromCurrency: Yup.string().required('From currency required'),
    fromAmount: Yup.number().required('From amount required'),
    toCurrency: Yup.string()
        .required('To currency required')
        .test('currencies-not-match', 'From and to currency must not match', function(value) {
			return this.parent.fromCurrency !== value
        }),
    toAmount: Yup.number()
        .required('To amount required'),
    contactFullName: Yup.string().required('Contact full name required'),
    contactEmail: Yup.string()
        .email('Email is not valid')
        .matches(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'Only valid email is allowed'
        )
        .required('Email is required'),
    status: Yup.string().required('Status required'),
})

const STATUSES = [
    'DRAFT',
    'ERROR',
    'PENDING',
    'WAITING_CONFIRMATION',
    'CONFIRMED',
    'FINISHED',
    'CANCELED',
]

const STATUS_OPTIONS = STATUSES.map(s => ({
    label: s.charAt(0) + s.substr(1, s.length).toLowerCase(),
    value: s
}))

const useStyles = makeStyles((theme) => ({
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	button: {
		margin: theme.spacing(3, 0, 2),
	},
}))

const OrderForm = ({
    autosubmit = false,
    currencyOptions = [],
    disabledInputs = {},
    initialValues = {},
    loading,
    onHandleCancel,
    onHandleSubmit,
}) => {
    let _date
    if (initialValues.date && moment(initialValues.date).isValid()) {
        _date = moment(initialValues.date).toDate()
    }

    const classes = useStyles()
    
    // States
    const [date, setDate] = React.useState(_date)

    const onChangeBaseRate = (baseRate, fromAmount, toAmount, setFieldValue) => {
        // Si sólo está definido baseRate y fromAmount -> calculamos toAmount
        if ((baseRate && fromAmount && !toAmount) || disabledInputs.baseRate === true) {
            toAmount = (fromAmount * baseRate).toFixed(2)
            setFieldValue('toAmount', Number(toAmount))
        }
        // Si sólo está definido baseRate y toAmount -> calculamos fromAmount
        else if ((baseRate && toAmount && !fromAmount) || disabledInputs.baseRate === true) {
            fromAmount = (toAmount / baseRate).toFixed(2)
            setFieldValue('fromAmount', Number(fromAmount))
        }
        // Si están definidos fromAmount y toAmount
        else if (disabledInputs.baseRate !== true && (toAmount / fromAmount).toFixed(2) !== Number(baseRate).toFixed(2)) {
            baseRate = (toAmount / fromAmount).toFixed(2)
        }
        if (disabledInputs.baseRate !== true && !isNaN(baseRate)) {
            setFieldValue('baseRate', Number(baseRate))
        }
    }

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={(values, extra) => {
                const { setSubmitting } = extra

                setSubmitting(false)
        
                // Reemplazar los valores de comienzo y fin a ISOString
                values.date = moment(date).toISOString()
        
                onHandleSubmit(values, extra)
              }}
            enableReinitialize
		>
			{({ handleBlur, handleChange, handleSubmit, setFieldValue, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
					{autosubmit && <AutoSubmit />}
                    {/* Input fin */}
                    <InputDateTimePicker
                        controlId="formDate"
                        label="Date"
                        name="date"
                        onChange={d => {
                            setDate(d)
                            if (initialValues.date) {
                                initialValues.date = d.toISOString()
                                setFieldValue('date', d.toISOString())
                            }
                        }}
                        value={date}
                    />
                    <GenericItemsSelector
                        controlId={`formFromCurrency`}
						label="From Currency"
                        name="fromCurrency"
                        native
						value={values.fromCurrency || ''}
                        disabled={disabledInputs.fromCurrency || false}
						required
						fullWidth
                        options={currencyOptions}
                        onHandleChange={e => {
                            setFieldValue('fromCurrency', e.target.value)
                            handleChange(e)
                        }}
                    />
					<InputField
						controlId={`formFromAmount`}
						label="From Amount"
						name="fromAmount"
						value={values.fromAmount || ''}
                        onHandleBlur={e => {
                            onChangeBaseRate(values.baseRate, e.target.value, values.toAmount, setFieldValue)
                            handleBlur(e)
                        }}
						required={true}
						type="number"
						fullWidth
					/>
					<GenericItemsSelector
                        controlId={`formToCurrency`}
						label="To Currency"
                        name="toCurrency"
                        native
						value={values.toCurrency || ''}
                        disabled={disabledInputs.toCurrency || false}
						required
						fullWidth
                        options={currencyOptions}
                        onHandleChange={e => {
                            setFieldValue('toCurrency', e.target.value)
                            handleChange(e)
                        }}
                    />
					<InputField
						controlId={`formToAmount`}
						label="To Amount"
						name="toAmount"
                        value={values.toAmount || ''}
                        onHandleBlur={e => {
                            onChangeBaseRate(values.baseRate, values.fromAmount, e.target.value, setFieldValue)
                            handleBlur(e)
                        }}
                        disabled={disabledInputs.toAmount || false}
						required
						type="number"
						fullWidth
					/>
                    {!disabledInputs.baseRate && (
                        <>
                            {/* Base rate */}
                            <InputField
                                controlId={`formBaseRate`}
                                label="Base Rate"
                                name="baseRate"
                                value={values.baseRate || ''}
                                onHandleBlur={e => {
                                    onChangeBaseRate(e.target.value, values.fromAmount, values.toAmount, setFieldValue)
                                    handleBlur(e)
                                }}
                                disabled={disabledInputs.baseRate || false}
                                required
                                type="number"
                                fullWidth
                            />
                            {/* Spread Percent */}
                            <InputField
                                controlId={`formSpreadPercent`}
                                label="Spread (%)"
                                type="number"
                                name="spreadPercent"
                                value={values.spreadPercent || ''}
                                fullWidth
                            />
                        </>
                    )}
                    <h3>Contact Information:</h3>
                    {/* Contact FullName */}
                    <InputField
						controlId={`formContactFullName`}
						label="Full Name"
						name="contactFullName"
						value={values.contactFullName || ''}
						required={true}
						fullWidth
					/>
                    {/* Contact Email */}
                    <InputField
						controlId={`formContactEmail`}
						label="Email"
						name="contactEmail"
						value={values.contactEmail || ''}
						required={true}
						fullWidth
					/>
                    {/* Contact FullName */}
                    <InputField
						controlId={`formContactPhone`}
						label="WhatsApp or Telegram"
                        placeholder="+5491123456789"
						name="contactPhone"
						value={values.contactPhone || ''}
						fullWidth
					/>
                    {/* Bank Info */}
                    <InputField
						controlId={`formToBankInfo`}
						label="Bank Info"
                        multiline
						name="toBankInfo"
                        rows={3}
						value={values.toBankInfo || ''}
						fullWidth
					/>
                    {/* Status */}
                    <GenericItemsSelector
                        controlId={`formStatus`}
						label="Status"
                        name="status"
                        native
						value={values.status || ''}
                        disabled={disabledInputs.status || false}
						required={true}
						fullWidth
                        options={STATUS_OPTIONS}
                        onHandleChange={e => {
                            setFieldValue('status', e.target.value)
                            handleChange(e)
                        }}
                    />
					{!autosubmit && (
                        <>
                            {values && values.baseRate && (
                                <p>
                                    <strong>Rate: <Fare amount={values.baseRate} symbol={values.toCurrency} /></strong>
                                </p>
                            )}
                            <Button 
                                variant="contained"
                                color="secondary"
                                className={classes.button}
                                type="button" 
                                onClick={onHandleCancel} 
                                style={{ marginRight: 10 }}>
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Save order
                            </Button>
                        </>
					)}
				</Form>
			)}
		</Formik>
	)
}

export default OrderForm
