import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

// Internal components
import AutoSubmit from '../Form/AutoSubmit'
import InputField from '../Form/InputField'
import GenericItemsSelector from '../Form/GenericItemsSelector'

const VALIDATION_SCHEMA = Yup.object({
    fromCurrency: Yup.string()
        .required('From currency required'),
    fromAmount: Yup.number()
        .required('From amount required'),
    toCurrency: Yup.string()
        .required('To currency required')
        .test('currencies-not-match', 'From and to currency must not match', function(value) {
			return this.parent.fromCurrency !== value
        }),
    toAmount: Yup.number()
        .required('To amount required'),
})

const useStyles = makeStyles((theme) => ({
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}))

const NewOrderForm = ({ autoSubmit = false, currencyOptions, initialValues, loading, onSubmit }) => {
	const classes = useStyles()

	if (!initialValues) {
		initialValues = {
			fromCurrency: '',
            fromAmount: '',
            toCurrency: '',
            toAmount: ''
		}
	}

	return (
		<Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={onSubmit}
		>
			{({ handleChange, handleSubmit, setFieldValue, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
					<AutoSubmit />
					{/* <InputField
						controlId={`formFromCurrency`}
						label="From Currency"
						name="fromCurrency"
						value={values.fromCurrency || ''}
						required={true}
						fullWidth
					/> */}
                    <GenericItemsSelector
                        controlId={`formFromCurrency`}
						label="From Currency"
                        name="fromCurrency"
                        native
						value={values.fromCurrency || ''}
						required={true}
						fullWidth
                        options={currencyOptions}
                        onHandleChange={e => {
                            setFieldValue('fromCurrency', e.target.value)
                            handleChange(e)
                        }}
                    />
					<InputField
						controlId={`formFromAmount`}
						label="From Amount"
						name="fromAmount"
						value={values.fromAmount || ''}
						required={true}
						type="number"
						fullWidth
					/>
					<GenericItemsSelector
                        controlId={`formToCurrency`}
						label="To Currency"
                        name="toCurrency"
                        native
						value={values.toCurrency || ''}
						required={true}
						fullWidth
                        options={currencyOptions}
                        onHandleChange={e => {
                            setFieldValue('toCurrency', e.target.value)
                            handleChange(e)
                        }}
                    />
                    {/* <InputField
						controlId={`formToCurrency`}
						label="To Currency"
						name="toCurrency"
						value={values.toCurrency || ''}
						required={true}
						fullWidth
					/> */}
					<InputField
						controlId={`formToAmount`}
						label="To Amount"
						name="toAmount"
                        value={values.toAmount || ''}
                        // readOnly
						required={true}
						type="number"
						fullWidth
					/>
					{!autoSubmit && (
						<Button
							fullWidth
							variant="contained"
							color="primary"
							className={classes.submit}
							onClick={handleSubmit}
							disabled={loading}
						>
							Send
						</Button>
					)}
				</Form>
			)}
		</Formik>
	)
}

export default NewOrderForm
