import React from 'react'
import moment from 'moment'

import Fare from '../Fare'
import GenericListTable from '../Table/GenericListTable'

function getFareValue(row, columnName) {
    const amount = row[columnName]
    const symbol = '$'

    return Fare({
        amount,
        symbol
    })
}


export default ({
    data: rows = [],
    onHandleClickRow = row => console.log(`Order data`, row),
}) => {
    const columns = [
        { name: 'date', title: 'Date', getCellValue: (row, columnName) => moment(row[columnName]).format('yyyy-MM-DD HH:mm') },
        { name: 'fromCurrency', title: 'From' },
        { name: 'fromAmount', title: 'From amount', getCellValue: getFareValue },
        { name: 'toCurrency', title: 'To' },
        { name: 'toAmount', title: 'To amount', getCellValue: getFareValue },
        { name: 'baseRate', title: 'Base rate', getCellValue: getFareValue },
        { name: 'spreadPercent', title: 'Spread (%)' },
        { name: 'contactFullName', title: 'Contact' },
        // { name: 'userId', title: 'User ID' },
        { name: 'status', title: 'Status' },
        { name: 'createdAt', title: 'Created At', getCellValue: (row, columnName) => moment(row[columnName]).format('yyyy-MM-DD HH:mm') },
    ]
    
    return (
        <GenericListTable 
            onClickRow={onHandleClickRow}
            columns={columns}
            rows={rows}
        />
    )
}