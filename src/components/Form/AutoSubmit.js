import React from 'react'
import { useFormikContext } from 'formik'

const AutoSubmitForm = () => {
	// Grab values and submitForm from context
	const { isValid, dirty, values, submitForm } = useFormikContext()

	React.useEffect(() => {
		// Submit the form imperatively as an effect as soon as form values.token are 6 digits long
		if (isValid && dirty) {
			submitForm()
		}
	}, [isValid, dirty, values, submitForm])
	return null
}

export default AutoSubmitForm