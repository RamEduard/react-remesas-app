import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import { FormControl, FormHelperText, InputLabel, Select } from '@material-ui/core'

/**
 * Service selector
 *
 * @param {any} props
 * @param {string} props.id
 * @param {string} props.label
 * @param {string} props.name
 * @param {Function} props.onHandleChange
 * @param {Array<{label: string, value: string}>} props.options
 */
const GenericItemsSelector = ({
	controlId,
	disabled,
	fullWidth,
	label,
	name,
	native,
	required,
	onHandleChange,
	options,
	value,
}) => (
	<Field name={name}>
		{({ field, form: { touched, errors } }) => (
			<FormControl error={errors && !!errors[name]} fullWidth={fullWidth}>
				<InputLabel htmlFor={controlId}>{`${label}`}</InputLabel>
				<Select
					{...field}
					native={native}
					onChange={onHandleChange}
					inputProps={{
						name,
						id: controlId,
					}}
					disabled={disabled}
					required={required}
					value={value}
				>
					<option value=""></option>
					{Array.isArray(options) &&
						options.map((op) => <option key={op.value} value={op.value}>{`${op.label}`}</option>)}
				</Select>
				{errors && touched[name] && !!errors[name] && (
					<FormHelperText id={`${controlId}-error-text`}>{`${errors[name]}`}</FormHelperText>
				)}
			</FormControl>
		)}
	</Field>
)

GenericItemsSelector.propTypes = {
	controlId: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	options: PropTypes.arrayOf(
		PropTypes.shape({
			label: PropTypes.string,
			value: PropTypes.string,
		})
	),
	onHandleChange: PropTypes.func,
}

export default GenericItemsSelector
