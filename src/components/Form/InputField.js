import React from 'react'
import { Field } from 'formik'
import {
	FormControl,
	FormHelperText,
	Input,
	InputLabel
} from '@material-ui/core'

/**
 * Input Field (email|number|tel|text)
 *
 * @param {string}  {controlId
 * @param {boolean} disabled
 * @param {boolean} fullWidth
 * @param {string}  label
 * @param {boolean} multiline
 * @param {string}  name
 * @param {func}    onHandleBlur
 * @param {func}    onHandleChange
 * @param {string}  placeholder
 * @param {boolean} readOnly
 * @param {boolean} required
 * @param {number}  rows
 * @param {string}  type
 * @param {string}  value}
 */
const InputField = ({ controlId, disabled, fullWidth, label, multiline, name, onHandleBlur, onHandleChange, placeholder, readOnly, required, rows, type, value }) => (
	<Field name={name}>
		{({ field, form: { errors } }) => (
			<FormControl error={errors && !!errors[name]} fullWidth={fullWidth}>
				<InputLabel htmlFor={controlId}>{`${label}`}</InputLabel>
				<Input
					{...field}
					id={controlId}
					placeholder={placeholder || ''}
					type={type}
					disabled={disabled}
					onBlur={onHandleBlur}
					onHandleChange={onHandleChange}
					required={required}
					value={value}
					readOnly={readOnly}
					multiline={multiline}
					rows={rows}
				/>
				{errors && !!errors[name] && (
					<FormHelperText id={`${controlId}-error-text`}>{`${errors[name]}`}</FormHelperText>
				)}
			</FormControl>
		)}
	</Field>
)

export default InputField
