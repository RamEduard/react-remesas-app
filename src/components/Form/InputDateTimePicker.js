import React from 'react'
import DatePicker from 'react-datepicker'
import { Field } from 'formik'
import {
	FormControl,
	FormHelperText,
	InputLabel
} from '@material-ui/core'
import 'react-datepicker/dist/react-datepicker.css'

const InputDateTimePicker = ({ controlId, disabled, label, name, value, onChange, fullWidth }) => {
    const [startDate, setStartDate] = React.useState(new Date())

    React.useEffect(() => {
        if (value) {
            setStartDate(value)
        }
    }, [value])

    return (
        <Field name={name}>
            {({ field, form: { touched, errors } }) => (
                <FormControl error={errors && touched[name] && !!errors[name]} fullWidth={fullWidth}>
				    {/* <InputLabel htmlFor={controlId}>{`${label}`}</InputLabel> */}
                    <DatePicker
                        // {...field}
                        selected={startDate}
                        onChange={date => {
                            onChange(date)
                            setStartDate(date)
                        }}
                        showTimeInput
                        timeInputLabel="Time:"
                        dateFormat="MM/dd/yyyy h:mm aa"
                        disabled={disabled}
                    />
                    {errors && touched[name] && !!errors[name] && (
                        <FormHelperText id={`${controlId}-error-text`}>{`${errors[name]}`}</FormHelperText>
                    )}
                </FormControl>
            )}
        </Field>
    )
}

export default InputDateTimePicker