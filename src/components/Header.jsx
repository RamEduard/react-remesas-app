import React from 'react'
import {
	AppBar,
	IconButton,
	makeStyles,
	Menu,
	MenuItem,
	Toolbar,
	Typography,
	Button,
} from '@material-ui/core'
import {
	AccountCircle,
	Home as HomeIcon,
} from '@material-ui/icons'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'

// Internal actions
import { logoutAction } from '../actions/userActions'

// Internal hooks
import useCurrentUser from '../hooks/useCurrentUser'

const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	appBar: {
		// position: 'relative',
		background: theme.palette.error.main,
		minHeight: 64,
	},
	appTitle: {
		color: '#000',
		cursor: 'pointer',
		flexGrow: 1,
		[theme.breakpoints.down('xs')]: {
			display: 'none'
		},
	},
	homeButton: {
		color: theme.palette.common.white,
		display: 'none',
		[theme.breakpoints.down('xs')]: {
			display: 'block',
			marginRight: theme.spacing(1)
		},
	},
	iconButton: {
		color: theme.palette.common.white,
		[theme.breakpoints.down('xs')]: {
			margin: 0,
			padding: 0,
		},
	},
	iconUserTypography: {
		[theme.breakpoints.down('xs')]: {
			display: 'none',
		},
	},
}))

/**
 * HeaderComponent
 */
const HeaderComponent = ({ logoutAction }) => {
    const classes = useStyles()
	const currentUser = useCurrentUser()
	const history = useHistory()

	// States
	const [anchorElOpenMenu, setAnchorElOpenMenu] = React.useState(null)
	
	/**
	 * Click for open menu
	 */
	const handleClickMenuAccount = (evt) => {
		if (currentUser !== null && currentUser.email) {
			setAnchorElOpenMenu(evt.currentTarget)
			return
		}
		history.push('/login')
	}

	const handleCloseMenuAccount = (url = '') => {
		setAnchorElOpenMenu(null)
		if (url && url !== '') {
			history.push(url)
		}
	}

	/**
	 * Handle logout
	 */
	const handleLogout = () => {
		setAnchorElOpenMenu(null)
		logoutAction().then(() => history.push('/login'))
	}

	return (
		<div className={classes.root}>
			<AppBar position="relative" className={classes.appBar}>
				<Toolbar>
					<Typography
						variant="h6"
						noWrap
						className={classes.appTitle}
						onClick={() => history.push('/')}
					>
						Remesas App
					</Typography>
					<IconButton component="span" className={classes.homeButton} onClick={() => history.push('/')}>
						<HomeIcon />
					</IconButton>
					{/* Acciones para sesión actual */}
					<Button
						aria-controls="current-account-menu"
						aria-haspopup="true"
						className={classes.iconButton}
						// color="primary"
						onClick={handleClickMenuAccount}
						startIcon={<AccountCircle />}
					>
						{currentUser !== null && currentUser.email && (
							<Typography
								className={classes.iconUserTypography}
							>{`${currentUser.firstName} ${currentUser.lastName}`}</Typography>
						)}
						{currentUser === null && (
							<Typography className={classes.iconUserTypography}>
								User account
							</Typography>
						)}
					</Button>
					<Menu
						id="current-account-menu"
						anchorEl={anchorElOpenMenu}
						keepMounted
						open={Boolean(anchorElOpenMenu)}
						onClose={handleCloseMenuAccount}
					>
						<MenuItem onClick={() => handleCloseMenuAccount('/dashboard')}>
							Dashboard
						</MenuItem>
						<MenuItem onClick={() => handleCloseMenuAccount('/arbitrage/calculator')}>
							Arbitrage Calculator
						</MenuItem>
						<MenuItem onClick={() => handleCloseMenuAccount('/orders')}>
							Orders
						</MenuItem>
						<MenuItem onClick={() => handleCloseMenuAccount('/transactions')}>
							Transactions
						</MenuItem>
						<MenuItem onClick={handleLogout}>Logout</MenuItem>
					</Menu>
				</Toolbar>
			</AppBar>
		</div>
	)
}

export default connect(null, {
	logoutAction,
})(HeaderComponent)
