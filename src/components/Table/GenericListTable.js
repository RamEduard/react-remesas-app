import React from 'react'
import {
  Grid,
  Table,
  TableHeaderRow,
} from '@devexpress/dx-react-grid-material-ui'
import { fade } from '@material-ui/core/styles/colorManipulator'
import { makeStyles } from '@material-ui/core/styles'
import { IntegratedSorting, SortingState } from '@devexpress/dx-react-grid'

const useStyles = makeStyles(theme => ({
  tableStriped: {
    '& tbody tr:nth-of-type(odd)': {
      backgroundColor: fade(theme.palette.primary.main, 0.15),
    },
  },
}))

const TableRow = ({ row, onClickRow, ...restProps }) => (
    <Table.Row
        {...restProps}
        // eslint-disable-next-line no-alert
        onClick={() => onClickRow(row)}
        style={{
            cursor: 'pointer'
        }}
    />
)

const TableComponentBase = props => {
    const classes = useStyles()

    return <Table.Table className={classes.tableStriped} {...props} />
}

const GenericListTable = ({ columns = [], rows = [], onClickRow }) => (
    <Grid
        rows={rows}
        columns={columns}
    >
      <SortingState defaultSorting={[{ columnName: 'createdAt', direction: 'asc' }]} />
      <IntegratedSorting />
      <Table
          rowComponent={props => <TableRow onClickRow={onClickRow} {...props} />}
          tableComponent={TableComponentBase}
      />
      <TableHeaderRow showSortingControls />
    </Grid>
)

export default GenericListTable