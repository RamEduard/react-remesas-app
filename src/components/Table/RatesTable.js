import React from 'react'
import PropTypes from 'prop-types'
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'

import Fare from '../Fare'

/**
 * Rates table component
 * @param {any} props
 * @param {Array} props.data
 */
const RatesTableComponent = ({ data }) => {
	return (
		<TableContainer component={Paper}>
			<Table aria-label="rates table">
				<TableHead>
					<TableCell>Currency</TableCell>
					<TableCell>Average</TableCell>
					<TableCell>Buy</TableCell>
					<TableCell>Sell</TableCell>
					<TableCell>Spread (%)</TableCell>
				</TableHead>
				<TableBody>
					<React.Fragment>
						{!data ||
							(data.length <= 0 && (
								<TableRow>
									<TableCell scope="row" colSpan={7} align="center">
										There are not rates loaded.
									</TableCell>
								</TableRow>
							))}
						{data &&
							data.length > 0 &&
							data.map((rate, i) => (
								<TableRow key={i}>
									<TableCell component="th" scope="row">
										{`${rate.currency}`}
									</TableCell>
									<TableCell>
										<Fare amount={rate.avg} symbol='$' />
									</TableCell>
									<TableCell>
										<Fare amount={rate.buy} symbol='$' />
									</TableCell>
									<TableCell>
										<Fare amount={rate.sell} symbol='$' />
									</TableCell>
									<TableCell>
										{`${rate.spread}%`}
									</TableCell>
								</TableRow>
							))}
					</React.Fragment>
				</TableBody>
			</Table>
		</TableContainer>
	)
}

RatesTableComponent.propTypes = {
	data: PropTypes.arrayOf(PropTypes.shape({
		currency: PropTypes.string.isRequired,
		avg: PropTypes.number.isRequired,
		buy: PropTypes.number.isRequired,
		sell: PropTypes.number.isRequired,
		spread: PropTypes.number.isRequired,
	}))
}

export default RatesTableComponent