import React from 'react'
import { Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

/**
 * Delete dialog component
 *
 * @param {object} props React props object
 */
const CustomDialog = ({
	open: openProp,
	onHandleClose = () => {},
	titleText = "",
	renderContent = () => (<></>),
	renderFooter = () => (<></>),
    size = "lg"
}) => {
  // States
  const [open, setOpen] = React.useState(false)

  // Efects
  React.useEffect(() => {
    if (openProp !== undefined) {
      setOpen(openProp);
    }
  }, [openProp])

  const handleClose = () => {
    setOpen(false)
    onHandleClose()
  }

  // Render
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{titleText}</DialogTitle>
      <DialogContent>
        {renderContent}
      </DialogContent>
      <DialogActions>
        {renderFooter}
      </DialogActions>
    </Dialog>
  )
}

export default CustomDialog