import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

// Internal components
import AutoSubmit from '../Form/AutoSubmit'
import InputField from '../Form/InputField'
import GenericItemsSelector from '../Form/GenericItemsSelector'

const TEMP_CURRENCIES = [
    'ARS',
    'CLP',
    'COP',
    'EUR',
    'PEN',
    'USD',
    'VES',
    'MXN',
    'BRL'
]

const CURRENCIES_ITEMS = TEMP_CURRENCIES.map(c => ({
    label: c,
    value: c
}))

const VALIDATION_SCHEMA = Yup.object({
    type: Yup.string().required('Type required'),
    amountBtc: Yup.number().required('Amount BTC required'),
    amountCurrency: Yup.number().required('Amount Currency required'),
    rate: Yup.number().required('Rate required'),
    currency: Yup.string().required('Currency required'),
    paymentMethod: Yup.string().required('Payment Method required'),
})

const useStyles = makeStyles((theme) => ({
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	button: {
		margin: theme.spacing(3, 0, 2),
	},
}))

/**
 * TransactionFormComponent
 * 
 * @param {object}   props
 * @param {boolean}  props.autosubmit
 * @param {object}   props.disabledInputs
 * @param {object}   props.initialValues
 * @param {boolean}  props.loading
 * @param {function} props.onHandleCancel
 * @param {function} props.onHandleSubmit
 */
const TransactionForm = ({
    autosubmit = false,
    disabledInputs = {},
    initialValues = {},
    loading,
    onHandleCancel,
    onHandleSubmit
}) => {
	const classes = useStyles()

    return (
        <Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={onHandleSubmit}
            enableReinitialize
		>
			{({ handleChange, handleSubmit, setFieldValue, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
                    {autosubmit && <AutoSubmit />}
                    <GenericItemsSelector
                        controlId={`formType`}
						label="Type"
                        name="type"
                        native
						value={values.type || ''}
						required={true}
						fullWidth
                        options={[
                            { label: 'Buy', value: 'BUY' },
                            { label: 'Sell', value: 'SELL' },
                        ]}
                        onHandleChange={e => {
                            setFieldValue('type', e.target.value)
                            handleChange(e)
                        }}
                    />
                    {/* Currency */}
                    <GenericItemsSelector
                        controlId={`formCurrency`}
						label="Currency"
                        name="currency"
                        native
						value={values.currency || ''}
						required={true}
						fullWidth
                        options={CURRENCIES_ITEMS}
                        onHandleChange={e => {
                            setFieldValue('currency', e.target.value)
                            handleChange(e)
                        }}
                    />
                    {/* Currency Label */}
                    <InputField
						controlId={`formCurrencyLabel`}
						label="Currency description (optional)"
						name="currencyLabel"
						value={values.currencyLabel || ''}
						type="text"
						fullWidth
					/>
                    {/* Amount currency */}
                    <InputField
						controlId={`formAmountCurrency`}
						label="Amount (Currency)"
						name="amountCurrency"
						value={values.amountCurrency || ''}
						required={true}
						type="number"
						fullWidth
					/>
                    {/* Amount in BTC */}
                    <InputField
						controlId={`formAmountBtc`}
						label="Amount (BTC)"
						name="amountBtc"
						value={values.amountBtc || ''}
						required={true}
						type="number"
						fullWidth
					/>
                    {/* Rate for transaction */}
					<InputField
						controlId={`formRate`}
						label="Rate"
						name="rate"
						value={values.rate || ''}
						required={true}
						type="number"
						fullWidth
					/>
                    {/* Payment method */}
                    <InputField
						controlId={`formPaymentMethod`}
						label="Payment Method"
						name="paymentMethod"
						value={values.paymentMethod || ''}
						required={true}
						type="text"
						fullWidth
					/>
                    {/* Payment method description (Opcional) */}
                    <InputField
						controlId={`formPaymentMethodDescription`}
						label="Payment Method Description (optional)"
						name="paymentMethodDescription"
						value={values.paymentMethodDescription || ''}
						type="text"
						fullWidth
					/>
					{!autosubmit && (
                        <>
                            <Button 
                                variant="contained"
                                color="secondary"
                                className={classes.button}
                                type="button" 
                                onClick={onHandleCancel} 
                                style={{ marginRight: 10 }}>
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Save transaction
                            </Button>
                        </>
					)}
				</Form>
			)}
		</Formik>
    )
}

export default TransactionForm