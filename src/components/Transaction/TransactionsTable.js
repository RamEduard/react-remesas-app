import React from 'react'
import moment from 'moment'

import Fare from '../Fare'
import GenericListTable from '../Table/GenericListTable'

function getFareValue(row, columnName) {
    const amount = row[columnName]
    const symbol = row['currency']

    return Fare({
        amount,
        symbol
    })
}

export default ({
    data: rows = [],
    onHandleClickRow = row => console.log(`Transaction data`, row),
}) => {
    const columns = [
        { name: 'type', title: 'Type' },
        { name: 'rate', title: 'Rate', getCellValue: getFareValue },
        { name: 'currency', title: 'Currency' },
        { name: 'paymentMethod', title: 'Payment Method' },
        { name: 'amountBtc', title: 'Amount (BTC)' },
        { name: 'amountCurrency', title: 'Amount currency', getCellValue: getFareValue },
        // { name: 'userId', title: 'User ID' },
        { name: 'createdAt', title: 'Created At', getCellValue: (row, columnName) => moment(row[columnName]).format('yyyy-MM-DD HH:mm') },
    ]
    
    return (
        <GenericListTable 
            onClickRow={onHandleClickRow}
            columns={columns}
            rows={rows}
        />
    )
}