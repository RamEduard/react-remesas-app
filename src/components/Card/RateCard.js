import React from 'react'
import { Card, CardContent, CardHeader, makeStyles, Typography } from '@material-ui/core'
import PropTypes from 'prop-types'

import Fare from '../Fare'

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 150,
        margin: theme.spacing(2)
    },
    title: {
        fontSize: 14
    }
}))

/**
 * RateCard component
 * 
 * @param {any} props
 * @param {string} props.label
 * @param {string} props.pair
 * @param {number} props.rate
 */
const RateCard = ({ label, pair, rate }) => {
    const classes = useStyles()

    return (
        <Card className={classes.root}>
            <CardHeader title={label} />
            <CardContent>
                <Typography variant="h5" component="h2">
                    <Fare amount={rate} symbol=" " />
                </Typography>
            </CardContent>
        </Card>
    )
}

RateCard.propTypes = {
    label: PropTypes.string.isRequired,
    pair: PropTypes.string.isRequired,
    rate: PropTypes.number.isRequired,
}

export default RateCard
