import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

// Internal components
import AutoSubmit from '../Form/AutoSubmit'
import InputField from '../Form/InputField'
import GenericItemsSelector from '../Form/GenericItemsSelector'

const VALIDATION_SCHEMA = Yup.object({
    service: Yup.string().required('Service required'),
    profitBtc: Yup.number().required('Profit - Amount in BTC required'),
    profitPercent: Yup.number().required('Profit percent required'),
    buyTransactionId: Yup.string().required('Buy transaction required'),
    sellTransactionId: Yup.string().required('Sell transaction required'),
})

const useStyles = makeStyles((theme) => ({
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	button: {
		margin: theme.spacing(3, 0, 2),
	},
}))

/**
 * ArbitrageTransactionFormComponent
 * 
 * @param {object}   props
 * @param {boolean}  props.autosubmit
 * @param {object}   props.disabledInputs
 * @param {object}   props.initialValues
 * @param {boolean}  props.loading
 * @param {function} props.onHandleCancel
 * @param {function} props.onHandleSubmit
 * @param {Array<any>} props.transactionOptions
 */
const ArbitrageTransactionForm = ({
    autosubmit = false,
    disabledInputs = {},
    initialValues = {},
    loading,
    onHandleCancel,
    onHandleSubmit,
    transactionOptions = []
}) => {
	const classes = useStyles()

    const buyTransactionOptions = transactionOptions.filter(o => o.type === 'BUY')
    const sellTransactionOptions = transactionOptions.filter(o => o.type === 'SELL')

    /**
     * Al cambiar cualquiera de las transacciones
     * 
     * @param {string} direction
     * @param {any} buyT 
     * @param {any} sellT 
     */
    const onTransactionsChange = (direction, buyT, sellT, setFieldValue) => {
        const buyTx = buyTransactionOptions.find(t => t.value === buyT)
        const sellTx = sellTransactionOptions.find(t => t.value === sellT)
        
        if (direction === 'BUY_SELL' && buyTx && sellTx) {
            const profitBtc = Number(buyTx.amountBtc - sellTx.amountBtc).toFixed(8)
            const profitPercent = Number(profitBtc / buyTx.amountBtc * 100).toFixed(2)

            setFieldValue('profitBtc', Number(profitBtc))
            setFieldValue('profitPercent', Number(profitPercent))
        } else if (direction === 'SELL_BUY' && buyTx && sellTx) {
            const profitBtc = Number(sellTx.amountBtc - buyTx.amountBtc).toFixed(8)
            const profitPercent = Number(profitBtc / sellTx.amountBtc * 100).toFixed(2)

            setFieldValue('profitBtc', Number(profitBtc))
            setFieldValue('profitPercent', Number(profitPercent))
        }
    }

    return (
        <Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={onHandleSubmit}
            enableReinitialize
		>
			{({ handleChange, handleSubmit, setFieldValue, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
                    {autosubmit && <AutoSubmit />}
                    {/* Flow arbitrage */}
                    <GenericItemsSelector
                        controlId={`formFlow`}
						label="Direction"
                        name="direction"
                        native
						value={values.direction || ''}
						required
						fullWidth
                        options={[
                            { label: 'BUY -> SELL', value: 'BUY_SELL' },
                            { label: 'SELL -> BUY', value: 'SELL_BUY' },
                        ]}
                        onHandleChange={e => {
                            setFieldValue('direction', e.target.value)
                            onTransactionsChange(e.target.value, values.buyTransactionId, values.sellTransactionId, setFieldValue)
                            handleChange(e)
                        }}
                    />
                    {/* Buy transaction */}
                    <GenericItemsSelector
                        controlId={`formBuyTransactionId`}
						label="Buy Transaction"
                        name="buyTransactionId"
                        native
						value={values.buyTransactionId || ''}
						required
						fullWidth
                        options={buyTransactionOptions}
                        onHandleChange={e => {
                            setFieldValue('buyTransactionId', e.target.value)
                            onTransactionsChange(values.direction, e.target.value, values.sellTransactionId, setFieldValue)
                            handleChange(e)
                        }}
                    />
                    {/* Sell transaction */}
                    <GenericItemsSelector
                        controlId={`formSellTransactionId`}
						label="Sell Transaction"
                        name="sellTransactionId"
                        native
						value={values.sellTransactionId || ''}
						required
						fullWidth
                        options={sellTransactionOptions}
                        onHandleChange={e => {
                            setFieldValue('sellTransactionId', e.target.value)
                            onTransactionsChange(values.direction, values.buyTransactionId, e.target.value, setFieldValue)
                            handleChange(e)
                        }}
                    />
                    {/* Currency Label */}
                    <InputField
						controlId={`formProfitBtc`}
						label="Profit - Amount in BTC"
						name="profitBtc"
						value={values.profitBtc || ''}
						type="number"
                        required
						fullWidth
					/>
                    {/* Amount currency */}
                    <InputField
						controlId={`formProfitPercent`}
						label="Profit percent (%)"
						name="profitPercent"
						value={values.profitPercent || ''}
						required
						type="number"
						fullWidth
					/>
					{!autosubmit && (
                        <>
                            <Button 
                                variant="contained"
                                color="secondary"
                                className={classes.button}
                                type="button" 
                                onClick={onHandleCancel} 
                                style={{ marginRight: 10 }}>
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Save arbitrage
                            </Button>
                        </>
					)}
				</Form>
			)}
		</Formik>
    )
}

export default ArbitrageTransactionForm