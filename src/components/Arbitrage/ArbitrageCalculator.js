import React from 'react'
import PropTypes from 'prop-types'
import { Button, FormControl, FormHelperText, InputLabel, Select, Grid, Typography } from '@material-ui/core'
import { Formik, Form, Field } from 'formik'
import InputField from '../Form/InputField'

const LOCAL_BITCOINS_FEE_PERCENT = 0.01 // 1%

/**
 * Arbitrage Calculator
 *
 * @param {*} props
 */
const ArbitrageCalculator = ({ classes, currencyItems, dataBtcAvg, onSubmit }) => {
	const [initialValues, setInitialValues] = React.useState({
		// From
		fromCurrencyCode: '',
		fromAvg: 0,
		fromBuy: 0,
		fromSell: 0,
		fromSpread: 0,
		fromSpreadPercent: 0,
		fromLocalBitcoinsFee: 0,
		fromAmount: 0,
		fromBtcAmount: 0,
		fromBtcAmountFinal: 0,
		// From
		toCurrencyCode: '',
		toAvg: 0,
		toBuy: 0,
		toSell: 0,
		toSpread: 0,
		toSpreadPercent: 0,
		toLocalBitcoinsFee: 0,
		toAvailableAmount: 0,
		toProposedRate: 0,
		toCustomRate: 0,
		toAmount: 0,
		toBtcAmount: 0,
		toBtcAmountFinal: 0,
		// Profits
		profit: 0,
		profitPercent: 0
	})

	const handleChangeFromCurrency = e => {
		const { value } = e.target

		if (value !== '' && dataBtcAvg.find(i => i.pair.includes(value))) {
			console.log('Currency found')
			const values = dataBtcAvg.find(i => i.pair.includes(value))
			const spread = (values.avg - (values.avg/values.buy*values.sell)) / values.avg
			setInitialValues({
				...initialValues,
				fromCurrencyCode: value,
				fromAvg: values.avg,
				fromBuy: values.buy,
				fromSell: values.sell,
				fromSpread: Math.round(spread * 100) / 100,
				fromSpreadPercent: Math.round(spread * 100 * 100) / 100,
				fromLocalBitcoinsFee: 0,
			})
		}
	}

	const handleChangeToCurrency = e => {
		const { value } = e.target

		if (value !== '' && value !== initialValues.fromCurrencyCode && dataBtcAvg.find(i => i.pair.includes(value))) {
			console.log('Currency found')
			const values = dataBtcAvg.find(i => i.pair.includes(value))
			const spread = (values.avg - (values.avg/values.buy*values.sell)) / values.avg

			setInitialValues({
				...initialValues,
				toCurrencyCode: value,
				toAvg: values.avg,
				toBuy: values.buy,
				toSell: values.sell,
				toSpread: Math.round(spread * 100) / 100,
				toSpreadPercent: Math.round(spread * 100 * 100) / 100,
				toLocalBitcoinsFee: 0,
			})
		}
	}

	const calculationsOnSubmit = values => {
		// Calculations

		// Total btc
		const fromBtcAmount = values.fromAmount / values.fromBuy
		const fromBtcAmountFinal = fromBtcAmount - (fromBtcAmount * LOCAL_BITCOINS_FEE_PERCENT)

		// Available amount
		const toAvailableAmount = fromBtcAmountFinal * values.toSell
		const toAvailableAmountFinal = Math.round((toAvailableAmount - (toAvailableAmount * LOCAL_BITCOINS_FEE_PERCENT)) * 100) / 100

		// Proposed rate
		const rateAproximated = Math.round(values.toSell / values.fromBuy * 100) / 100
		const toProposedRate = rateAproximated - (rateAproximated * LOCAL_BITCOINS_FEE_PERCENT)
		let toAmount = values.fromAmount * toProposedRate

		// Replace toAmount
		if (values.toCustomRate > 0) {
			toAmount = values.fromAmount * values.toCustomRate
		}

		// TODO: LB Fees

		// Btc used
		const toBtcAmount = toAmount / values.toSell
		const toBtcAmountFinal = toBtcAmount - (toBtcAmount * LOCAL_BITCOINS_FEE_PERCENT)

		// Profits
		const profit = fromBtcAmount - toBtcAmountFinal
		const profitPercent = Math.round(profit / fromBtcAmount * 100 * 100) / 100

		const newValues = {
			fromAmount: values.fromAmount,
			fromBtcAmount: fromBtcAmount.toFixed(8),
			fromBtcAmountFinal: fromBtcAmountFinal.toFixed(8),
			toAvailableAmount: toAvailableAmountFinal,
			toCustomRate: values.toCustomRate,
			toProposedRate: toProposedRate.toFixed(2),
			toAmount,
			toBtcAmount: toBtcAmount.toFixed(8),
			toBtcAmountFinal: toBtcAmountFinal.toFixed(8),
			profit: profit.toFixed(8),
			profitPercent
		}

		setInitialValues({
			...initialValues,
			...newValues
		})

		onSubmit({ ...values, ...newValues })
	}

	return (
		<Formik
			initialValues={initialValues}
			// validationSchema={VALIDATION_SCHEMA}
			onSubmit={calculationsOnSubmit}
			enableReinitialize
		>
			{({ handleChange, handleSubmit, values }) => (
				<Form onSubmit={handleSubmit}>
					{/* Send money */}
					<Typography component="h2" variant="h5" style={{ margin: '15px 0', borderBottom: '1px dotted #999' }}>
            Buy
					</Typography>
					<Grid container spacing={2}>
						<Grid item md={2}>
							<Field name='fromCurrencyCode'>
								{({ field, form: { errors } }) => (
									<FormControl className={classes.formControl}>
										<InputLabel htmlFor="select-from-currency-code">Select Currency</InputLabel>
										<Select
											{...field}
											native
											value={values.fromCurrencyCode}
											onChange={e => {
												handleChangeFromCurrency(e)
												handleChange(e)
											}}
											inputProps={{
												name: 'fromCurrencyCode',
												id: 'select-from-currency-code',
											}}
										>
											<option aria-label="None" value="" />
											{currencyItems &&
												currencyItems.map((op) => <option key={op.value} value={op.value}>{`${op.label}`}</option>)}
										</Select>
										{errors && !!errors['fromCurrencyCode'] && (
											<FormHelperText id={`select-from-currency-code-error-text`}>{`${errors['fromCurrencyCode']}`}</FormHelperText>
										)}
									</FormControl>
								)}
							</Field>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formFromAvg`}
								label="Rate Avg"
								name="fromAvg"
								value={values.fromAvg || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formFromBuy`}
								label="Rate Buy"
								name="fromBuy"
								value={values.fromBuy || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formFromSell`}
								label="Rate Sell"
								name="fromSell"
								value={values.fromSell || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={1}>
							<InputField
								controlId={`formFromSpread`}
								label="Spread"
								name="fromSpread"
								value={values.fromSpread || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
						<Grid item md={1}>
							<InputField
								controlId={`formFromSpreadPercent`}
								label="Spread (%)"
								name="fromSpreadPercent"
								value={values.fromSpreadPercent || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formFromLocalBitcoinsFee`}
								label="LocalBitcoins Fee (1%)"
								name="fromLocalBitcoinsFee"
								value={values.fromLocalBitcoinsFee || ''}
								required
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
					</Grid>
					{/* Receive money */}
					<Typography component="h2" variant="h5" style={{ margin: '15px 0', borderBottom: '1px dotted #999' }}>
            Sell
					</Typography>
					<Grid container spacing={2}>
						<Grid item md={2}>
							<Field name='toCurrencyCode'>
								{({ field, form: { errors } }) => (
									<FormControl className={classes.formControl}>
										<InputLabel htmlFor="select-to-currency-code">Select Currency</InputLabel>
										<Select
											{...field}
											native
											value={values.toCurrencyCode}
											onChange={e => {
												handleChangeToCurrency(e)
												handleChange(e)
											}}
											inputProps={{
												name: 'toCurrencyCode',
												id: 'select-to-currency-code',
											}}
										>
											<option aria-label="None" value="" />
											{currencyItems &&
												currencyItems.map((op) => <option key={op.value} value={op.value}>{`${op.label}`}</option>)}
										</Select>
										{errors && !!errors['toCurrencyCode'] && (
											<FormHelperText id={`select-to-currency-code-error-text`}>{`${errors['toCurrencyCode']}`}</FormHelperText>
										)}
									</FormControl>
								)}
							</Field>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToAvg`}
								label="Rate Avg"
								name="toAvg"
								value={values.toAvg || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToBuy`}
								label="Rate Buy"
								name="toBuy"
								value={values.toBuy || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToSell`}
								label="Rate Sell"
								name="toSell"
								value={values.toSell || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={1}>
							<InputField
								controlId={`formToSpread`}
								label="Spread"
								name="toSpread"
								value={values.toSpread || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
						<Grid item md={1}>
							<InputField
								controlId={`formToSpreadPercent`}
								label="Spread (%)"
								name="toSpreadPercent"
								value={values.toSpreadPercent || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToLocalBitcoinsFee`}
								label="LocalBitcoins Fee (1%)"
								name="toLocalBitcoinsFee"
								value={values.toLocalBitcoinsFee || ''}
								required
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
					</Grid>
					<Typography component="h6" variant="subtitle1" style={{ margin: '15px 0', marginTop: 30, borderBottom: '1px dotted #999' }}>
            Send
					</Typography>
					<Grid container spacing={2}>
						<Grid item md={2}>
							<InputField
								controlId={`formAmount`}
								label="Amount to send"
								name="fromAmount"
								value={values.fromAmount || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={3}>
							<InputField
								controlId={`formFromBtcAmount`}
								label="Btc Amount"
								name="fromBtcAmount"
								value={values.fromBtcAmount || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={3}>
							<InputField
								controlId={`formFromBtcAmountFinal`}
								label="Btc Amount (Obtained)"
								name="fromBtcAmountFinal"
								value={values.fromBtcAmountFinal || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
					</Grid>
					<Typography component="h6" variant="subtitle1" style={{ margin: '15px 0', borderBottom: '1px dotted #999' }}>
            Receive
					</Typography>
					<Grid container spacing={2}>
						<Grid item md={2}>
							<InputField
								controlId={`formAvailableAmount`}
								label="Available amount (aprox)"
								name="toAvailableAmount"
								value={values.toAvailableAmount || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToProposedRate`}
								label="Proposed rate"
								name="toProposedRate"
								value={values.toProposedRate || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToCustomRate`}
								label="Custom rate"
								name="toCustomRate"
								value={values.toCustomRate || ''}
								required
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={2}>
							<InputField
								controlId={`formToAmount`}
								label="Amount to receive"
								name="toAmount"
								value={values.toAmount || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={3}>
							<InputField
								controlId={`formToBtcAmount`}
								label="Btc Amount"
								name="toBtcAmount"
								value={values.toBtcAmount || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
						<Grid item md={3}>
							<InputField
								controlId={`formToBtcAmountFinal`}
								label="Btc Amount (Sold)"
								name="toBtcAmountFinal"
								value={values.toBtcAmountFinal || ''}
								disabled
								type="number"
								fullWidth
							/>
						</Grid>
					</Grid>
					{/* Profits */}
					<Typography component="h2" variant="h5" style={{ margin: '15px 0', marginTop: 30, borderBottom: '1px dotted #999' }}>
            Profits
					</Typography>
					<Grid container spacing={2}>
						<Grid item md={2}>
							<InputField
								controlId={`formProfit`}
								label="Profit"
								name="profit"
								value={values.profit || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
						<Grid item md={1}>
							<InputField
								controlId={`formProfitPercent`}
								label="Profit (%)"
								name="profitPercent"
								value={values.profitPercent || ''}
								type="number"
								disabled
								fullWidth
							/>
						</Grid>
					</Grid>
					<Grid container spacing={2}>
						<Grid item>
							<Button
								variant="contained"
								color="primary"
								onClick={handleSubmit}
								style={{marginTop: 15}}
							>
								Get Calculations
							</Button>
						</Grid>
					</Grid>
				</Form>
			)}
		</Formik>
	)
}

ArbitrageCalculator.propTypes = {
	currencyItems: PropTypes.arrayOf(
		PropTypes.shape({
			label: PropTypes.string.isRequired,
			value: PropTypes.string.isRequired,
		})
	),
}

export default ArbitrageCalculator
