import React from 'react'
import moment from 'moment'

import GenericListTable from '../Table/GenericListTable'

export default ({
    data: rows = [],
    onHandleClickRow = row => console.log(`ArbitrageTransaction data`, row),
}) => {
    const columns = [
        { name: 'service', title: 'Service' },
        { name: 'profitBtc', title: 'Profit BTC' },
        { name: 'profitPercent', title: 'Profit (%)' },
        // { name: 'userId', title: 'User ID' },
        { name: 'createdAt', title: 'Created At', getCellValue: (row, columnName) => moment(row[columnName]).format('yyyy-MM-DD HH:mm') },
    ]
    
    return (
        <GenericListTable 
            onClickRow={onHandleClickRow}
            columns={columns}
            rows={rows}
        />
    )
}