import React from 'react'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import moment from 'moment'

// Internal components
import AutoSubmit from '../Form/AutoSubmit'
import InputDateTimePicker from '../Form/InputDateTimePicker'
import InputField from '../Form/InputField'
import GenericItemsSelector from '../Form/GenericItemsSelector'

const STATUSES = [
    'DRAFT',
    'ERROR',
    'PENDING',
    'WAITING_CONFIRMATION',
    'CONFIRMED',
    'FINISHED',
    'CANCELED',
]

const STATUS_OPTIONS = STATUSES.map(s => ({
    label: s.charAt(0) + s.substr(1, s.length).toLowerCase(),
    value: s
}))

const VALIDATION_SCHEMA = Yup.object({
    currency: Yup.string().required('Currency required'),
    amount: Yup.number().required('Amount required'),
    bankDescription: Yup.string().required('Bank description required'),
    status: Yup.string().required('Status required'),
})

const useStyles = makeStyles((theme) => ({
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	button: {
		margin: theme.spacing(3, 0, 2),
	},
}))

/**
 * BankTransferForm component
 * 
 * @param {object}   props
 * @param {boolean}  props.autosubmit
 * @param {object}   props.disabledInputs
 * @param {object}   props.initialValues
 * @param {boolean}  props.loading
 * @param {function} props.onHandleCancel
 * @param {function} props.onHandleSubmit
 */
const BankTransferForm = ({
    autosubmit = false,
    currencyOptions = [],
    disabledInputs = {},
    initialValues = {},
    loading,
    onHandleCancel,
    onHandleSubmit
}) => {
    let _date
    if (initialValues.date && moment(initialValues.date).isValid()) {
        _date = moment(initialValues.date).toDate()
    }

	const classes = useStyles()

    // States
    const [date, setDate] = React.useState(_date)

    return (
        <Formik
			initialValues={initialValues}
			validationSchema={VALIDATION_SCHEMA}
			onSubmit={(values, extra) => {
                const { setSubmitting } = extra

                setSubmitting(false)
        
                // Reemplazar los valores de comienzo y fin a ISOString
                values.date = moment(date).toISOString()
        
                onHandleSubmit(values, extra)
            }}
            enableReinitialize
		>
			{({ handleChange, handleSubmit, setFieldValue, values }) => (
				<Form className={classes.form} onSubmit={handleSubmit}>
                    {autosubmit && <AutoSubmit />}
                    <InputDateTimePicker
                        controlId="formDate"
                        label="Date"
                        name="date"
                        onChange={d => {
                            setDate(d)
                            if (initialValues.date) {
                                initialValues.date = d.toISOString()
                                setFieldValue('date', d.toISOString())
                            }
                        }}
                        value={date}
                    />
                    {/* Currency */}
                    <GenericItemsSelector
                        controlId={`formCurrency`}
						label="Currency"
                        name="currency"
                        native
						value={values.currency || ''}
						required
						fullWidth
                        options={currencyOptions}
                        onHandleChange={e => {
                            setFieldValue('currency', e.target.value)
                            handleChange(e)
                        }}
                    />
                    
                    {/* Amount currency */}
                    <InputField
						controlId={`formAmount`}
						label="Amount"
						name="amount"
						value={values.amount || ''}
						required
						type="number"
						fullWidth
					/>
                    {/* Bank Description */}
                    <InputField
						controlId={`formBankDescription`}
						label="Bank description"
						name="bankDescription"
						value={values.bankDescription || ''}
						required
                        multiline
                        rows={3}
						type="text"
						fullWidth
					/>
                    {/* Image url */}
                    <InputField
						controlId={`formImageUrl`}
						label="Image URL"
						name="imageUrl"
						value={values.imageUrl || ''}
						type="text"
						fullWidth
					/>
                    {/* Status */}
                    <GenericItemsSelector
                        controlId={`formStatus`}
						label="Status"
                        name="status"
                        native
						value={values.status || ''}
                        disabled={disabledInputs.status || false}
						required={true}
						fullWidth
                        options={STATUS_OPTIONS}
                        onHandleChange={e => {
                            setFieldValue('status', e.target.value)
                            handleChange(e)
                        }}
                    />
					{!autosubmit && (
                        <>
                            <Button 
                                variant="contained"
                                color="secondary"
                                className={classes.button}
                                type="button" 
                                onClick={onHandleCancel} 
                                style={{ marginRight: 10 }}>
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.button}
                                onClick={handleSubmit}
                                disabled={loading}
                            >
                                Save bank transfer
                            </Button>
                        </>
					)}
				</Form>
			)}
		</Formik>
    )
}

export default BankTransferForm