import moment from 'moment'

import Fare from '../Fare'
import GenericListTable from '../Table/GenericListTable'

function getFareValue(row, columnName) {
    const amount = row[columnName]
    const symbol = row['currency']

    return Fare({
        amount,
        symbol
    })
}

export default ({
    data: rows = [],
    onHandleClickRow = row => console.log(`BankTransfer data`, row),
}) => {
    const columns = [
        { name: 'amount', title: 'Amount', getCellValue: getFareValue },
        { name: 'currency', title: 'Currency' },
        { name: 'bankDescription', title: 'Bank Description' },
        { name: 'imageUrl', title: 'URL Image' },
        { name: 'status', title: 'Status' },
        // { name: 'userId', title: 'User ID' },
        { name: 'createdAt', title: 'Created At', getCellValue: (row, columnName) => moment(row[columnName]).format('yyyy-MM-DD HH:mm') },
    ]
    
    return (
        <GenericListTable 
            onClickRow={onHandleClickRow}
            columns={columns}
            rows={rows}
        />
    )
}