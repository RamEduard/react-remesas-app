import {
	// Login
	USER_LOGIN_ERROR,
	USER_LOGIN_SUCCESS,
	// Register
	USER_REGISTER_LOADING,
	USER_REGISTER_ERROR,
	USER_REGISTER_SUCCESS,
	// Current User
	SET_USER_INFO_SUCCESS,
	USER_LOGOUT_SUCCESS,
} from '../redux/types'

import config from '../config'

let token

/**
 * Return JWT Token
 */
export const getAuthToken = async () => {
	if (token) {
		return Promise.resolve(token)
	}

	if (process.browser) {
		token = localStorage.getItem(config.authToken)
	}
	return Promise.resolve(token)
}

/**
 * Set logged in action
 * 
 * @param {string} token 
 */
export const loggedInAction = token => async (dispatch) => {
	if (!token || token === '') {
		dispatch({
			type: USER_LOGIN_ERROR,
			payload: {
				error: 'Something wrong has occurred when trying to log in.'
			}
		})
		return
	}

	dispatch({
		type: USER_LOGIN_SUCCESS,
		payload: { token },
	})
	localStorage.setItem(config.authToken, token)
}

/**
 * Redux loginAction
 */
export const logoutAction = () => async (dispatch) => {
	localStorage.setItem(config.authToken, '')
	// dispatch success
	dispatch({ type: USER_LOGOUT_SUCCESS })
}

/**
 * Redux registerAction
 *
 * @param {string} name
 * @param {string} email
 * @param {string} password
 */
export const registerAction = (name, email, password) => async (dispatch) => {
	// Loading products
	dispatch({ type: USER_REGISTER_LOADING })

	try {
		const registerData = {
			data: {}
		}

		if (!registerData || !registerData.data || !registerData.data.user)
			throw new Error('User not registed')
		// Loaded products
		dispatch({
			type: USER_REGISTER_SUCCESS,
			payload: { user: registerData && registerData.data && registerData.data.user },
		})
	} catch (e) {
		console.log(e)
		const error = `Something wrong has occurred. Details: ${e.message}`
		// Loaded products
		dispatch({
			type: USER_REGISTER_ERROR,
			payload: { error },
		})
	}
}

/**
 * Redux getCurrentUserAction
 */
export const setUserInfoAction = userInfo => async (dispatch) => {
	// Loaded products
	dispatch({
		type: SET_USER_INFO_SUCCESS,
		payload: { user: userInfo },
	})
}
