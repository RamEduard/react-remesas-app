const PRODUCTION = {
	apiUrl: 'https://api.remesasbtc.xyz',
	url: 'https://remesasbtc.xyz',
	authToken: 'JWT_AUTH_TOKEN'
}

const TEST = {
	apiUrl: 'http://198.74.53.105:8080',
	url: 'http://198.74.53.105:3000',
	authToken: 'JWT_AUTH_TOKEN'
}

const DEVELOPMENT = {
	apiUrl: 'http://localhost:8080',
	url: 'http://localhost:3000',
	authToken: 'JWT_AUTH_TOKEN'
}

export default process.env.NODE_ENV === "production"
  ? PRODUCTION
  : process.env.NODE_ENV === "test"
  ? TEST
  : DEVELOPMENT
