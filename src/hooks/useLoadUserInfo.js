import React from 'react'
import { useLazyQuery } from '@apollo/react-hooks'

// Queries
import { GET_USER_INFO } from '../graphql/queries'

export default () => {
    const mounted = React.useRef(false)

    // Queries
	const [getUserInfo, { data: dataUserInfo }] = useLazyQuery(GET_USER_INFO)

    React.useEffect(() => {
        if (!mounted.current) {
            getUserInfo()
            mounted.current = true
        }
        return () => {
            mounted.current = false
        }
    }, [])

    return dataUserInfo
}