import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import LayoutComponent from './components/Layout'

// Containers
import DashboardContainer from './containers/Dashboard'
import HomeContainer from './containers/Home'
import LoginContainer from './containers/Login'
import RegisterContainer from './containers/Register'
import ArbitrageCalculatorContainer from './containers/ArbitrageCalculator'
import OrdersContainer from './containers/Orders'
import OrderDetailsContainer from './containers/Orders/OrderDetails'
import OrderUserContainer from './containers/Orders/OrderUser'
import TransactionsContainer from './containers/Transactions'
import TransactionDetailsContainer from './containers/Transactions/TransactionDetails'
// import NotFoundContainer from './containers/Error/NotFound'

const ROUTES = [
	{
		path: '/',
		Component: HomeContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/dashboard',
		Component: DashboardContainer,
		useLayout: true,
        isPrivate: true
	},
	{
		path: '/login',
		Component: LoginContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/register',
		Component: RegisterContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/arbitrage/calculator',
		Component: ArbitrageCalculatorContainer,
        useLayout: true,
        isPrivate: false
	},
	// Orders
	{
		path: '/orders',
		Component: OrdersContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/order/:orderId',
		Component: OrderDetailsContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/order/:orderId/:token',
		Component: OrderUserContainer,
        useLayout: true,
        isPrivate: false
	},
	// Transactions
	{
		path: '/transactions',
		Component: TransactionsContainer,
        useLayout: true,
        isPrivate: false
	},
	{
		path: '/transaction/:transactionId',
		Component: TransactionDetailsContainer,
        useLayout: true,
        isPrivate: false
	},
]

const RoutesComponent = () => (
	<BrowserRouter>
		<Switch>
			{ROUTES.map(({ Component, path, Props, useLayout }) => {
				if (useLayout) {
					return <Route
						exact
						key={path}
						path={path}
						render={props => (
							<LayoutComponent>
								<Component {...props} {...Props} />
							</LayoutComponent>
						)}
					/>
				}
				return <Route exact key={path} path={path} render={props => <Component {...props} {...Props} />} />
			})}
		</Switch>
	</BrowserRouter>
)

export default RoutesComponent
