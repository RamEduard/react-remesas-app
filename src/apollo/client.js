import { ApolloClient, InMemoryCache } from '@apollo/client'
import { ApolloLink } from '@apollo/client/link/core'
import { setContext } from '@apollo/client/link/context'
import { onError } from '@apollo/client/link/error'
import { HttpLink } from '@apollo/client/link/http'
import { createPersistedQueryLink } from '@apollo/client/link/persisted-queries'
import { sha256 } from 'crypto-hash'

import { getAuthToken } from '../actions/userActions'

import config from '../config'

/**
 * Auth link set authorization header
 */
const authLink = setContext(async (req, { headers }) => {
	const token = await getAuthToken()
	let auth = {}

	if (token) {
		auth = { Authorization: `Bearer ${token}` }
	}

	return {
		headers: {
			...headers,
			...auth
		}
	}
})

/**
 * When some error has occurred is logged in console
 */
const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors)
		graphQLErrors.map(({ message, location, path }) => {
			// if (process.env.NODE_ENV !== 'production') {
			    console.log(`[GraphQL error]: Message: ${message}, Location: ${location}, Path: ${path}`)
            // }
            return `[GraphQL error]: Message: ${message}, Location: ${location}, Path: ${path}`
		})
	if (networkError) {
		// if (process.env.NODE_ENV !== 'production') {
		    console.log(`[Network error]: ${networkError}`)
		// }
	}
})

/**
 * 
 */
export const queryOrMutationLink = (config = {}) =>
	createPersistedQueryLink({ sha256, useGETForHashedQueries: true }).concat(
		new HttpLink({
			...config,
			credentials: 'same-origin'
		})
	)

const linksClient = () => {
	return [
        authLink,
        errorLink,
        queryOrMutationLink({
            fetch,
            uri: `${config.apiUrl}/graphql`
        })
    ]
}

// Delete __typename when it is a mutation
export const middleWareLink = new ApolloLink((operation, forward) => {
	if (operation.variables) {
	  const omitTypename = (key, value) => (key === '__typename' ? undefined : value);
	  // eslint-disable-next-line no-param-reassign
	  operation.variables = JSON.parse(JSON.stringify(operation.variables), omitTypename);
	}
	return forward(operation);
})

export const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloLink.from([middleWareLink, ...linksClient()])
})