import { gql } from '@apollo/client'

export const SIGN_IN = gql`
	mutation signin($email: String!, $password: String!) {
		signin(email: $email, password: $password) {
			expirationDate
			token
		}
	}
`

export const SIGN_UP = gql`
	mutation signup($user: InputUser) {
		signup(user: $user) {
			saved
			message
		}
	}
`

export const ORDER_CREATE = gql`
	mutation orderCreate($input: InputOrder!) {
		orderCreate (input: $input) {
			_id
			token
			date
			status
			fromCurrency
			toCurrency
			baseRate
			fromAmount
			toAmount
			toBankInfo
			spreadPercent
			contactEmail
			contactPhone
			contactFullName
			# User Seller
			user {
				firstName
				lastName
				email
			}
			# User Client
			userClient {
				firstName
				lastName
				email
			}
			createdAt
			updatedAt
		}
	}
`

export const ORDER_UPDATE = gql`
	mutation orderUpdate($input: InputOrder!) {
		orderUpdate (input: $input) {
			_id
			token
			date
			status
			fromCurrency
			toCurrency
			baseRate
			fromAmount
			toAmount
			toBankInfo
			spreadPercent
			contactEmail
			contactPhone
			contactFullName
			# User Seller
			user {
				firstName
				lastName
				email
			}
			# User Client
			userClient {
				firstName
				lastName
				email
			}
			createdAt
			updatedAt
		}
	}
`

export const ORDER_DELETE = gql`
	mutation orderDelete($_id: String) {
		orderDelete(_id: $_id)
	}
`

export const TRANSACTION_CREATE = gql`
	mutation transactionCreate($input: InputTransaction!) {
		transactionCreate (input: $input) {
			_id
			type
			rate
			currency
			currencyLabel
			amountBtc
			amountCurrency
			paymentMethod
			paymentMethodDescription
			images
			user {
			firstName
			lastName
			email
			}
			userId
			createdAt
			updatedAt
		}
	}
`

export const TRANSACTION_UPDATE = gql`
	mutation transactionUpdate($input: InputTransaction!) {
		transactionUpdate (input: $input) {
			_id
			type
			rate
			currency
			currencyLabel
			amountBtc
			amountCurrency
			paymentMethod
			paymentMethodDescription
			images
			user {
				firstName
				lastName
				email
			}
			userId
			createdAt
			updatedAt
		}
	}
`

export const TRANSACTION_DELETE = gql`
	mutation transactionDelete($_id: String) {
		transactionDelete(_id: $_id)
	}
`

export const ARBITRAGE_TRANSACTION_CREATE = gql`
	mutation arbitrageTransactionCreate($input: InputArbitrageTransaction!) {
		arbitrageTransactionCreate(input: $input) {
			_id
			service
			direction
			buyTransactionId
			sellTransactionId
			orderId
			profitBtc
			profitPercent
			userId
		}
	}
`
export const ARBITRAGE_TRANSACTION_UPDATE = gql`
	mutation arbitrageTransactionUpdate($input: InputArbitrageTransaction!) {
		arbitrageTransactionUpdate (input: $input) {
			_id
			service
			direction
			buyTransactionId
			sellTransactionId
			orderId
			profitBtc
			profitPercent
			userId
		}
	}
`

export const ARBITRAGE_TRANSACTION_DELETE = gql`
	mutation arbitrageTransactionDelete($_id: String) {
		arbitrageTransactionDelete(_id: $_id)
	}
`

export const BANK_TRANSFER_CREATE = gql`
	mutation bankTransferCreate($input: InputBankTransfer!) {
		bankTransferCreate(input: $input) {
			_id
			amount
			bankDescription
			currency
			date
			imageUrl
			status
			transactionId
			userId
		}
	}
`
export const BANK_TRANSFER_UPDATE = gql`
	mutation bankTransferUpdate($input: InputBankTransfer!) {
		bankTransferUpdate (input: $input) {
			_id
			amount
			bankDescription
			currency
			date
			imageUrl
			status
			transactionId
			userId
		}
	}
`

export const BANK_TRANSFER_DELETE = gql`
	mutation bankTransferDelete($_id: String) {
		bankTransferDelete(_id: $_id)
	}
`