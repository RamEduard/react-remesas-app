import { gql } from '@apollo/client'

export const GET_USER_INFO = gql`
	query {
		userInfo {
			firstName
			lastName
			email
		}
	}
`

export const GET_RATES_BY_CURRENCY = gql`
	query ratesByCurrency($currencyCode: String!) {
		ratesByCurrency(currencyCode: $currencyCode) {
			avg {
				currency
				avg_6h
				avg_12h
				avg_24h
				rates {
					last
				}
				volume_btc
			}
			buy {
				avg
				first
				min
				max
				avg_usd
				min_usd
				max_usd
			}
			sell {
				avg
				first
				min
				max
				avg_usd
				min_usd
				max_usd
			}
			spread {
				avg
				first
				min
				max
				avg_usd
				min_usd
				max_usd
			}
		}
	}
`

export const GET_DASHBOARD = gql`
	query {
		dashboard {
			btcRates {
				label
				pair
				rate
			}
		}
	}
`

export const GET_HOURLY_RATES = gql`
	query hourlyRatesList($filters: InputHourlyRateFilters) {
		hourlyRatesList (filters: $filters) {
			hasNext
			nextPage
			items {
			_id
			date
			pair
			service
			avg
			buy
			sell
			createdAt
			updatedAt
			}
			pages
			total
		}
	}
`

export const GET_ORDER_LIST = gql`
	query orderList(
		$filters: InputOrderFilters
	) {
		ordersList (
			filters: $filters
		) {
			hasNext
			nextPage
			pages
			total
			items {
				_id
				# token
				date
				status
				fromCurrency
				toCurrency
				baseRate
				fromAmount
				toAmount
				toBankInfo
				spreadPercent
				contactEmail
				contactPhone
				contactFullName
				# User Seller
				user {
					firstName
					lastName
					email
				}
				# User Client
				userClient {
					firstName
					lastName
					email
				}
				createdAt
				updatedAt
			}
		}
	}
`

export const GET_ORDER = gql`
	query orderGet($_id: String!) {
		orderGet(_id: $_id) {
			_id
			token
			date
			status
			fromCurrency
			toCurrency
			baseRate
			fromAmount
			toAmount
			toBankInfo
			spreadPercent
			contactEmail
			contactPhone
			contactFullName
			user {
				firstName
				lastName
				email
			}
			userClient {
				firstName
				lastName
				email
			}
			transactions {
				_id
				type
				rate
				currency
				currencyLabel
				amountBtc
				amountCurrency
				paymentMethod
				paymentMethodDescription
				images
				user {
					firstName
					lastName
					email
				}
				userId
				createdAt
				updatedAt
			}
			createdAt
			updatedAt
		}
		}
`

export const GET_ORDER_BY_ID_TOKEN = gql`
	query orderByIdAndToken($_id: String!, $token: String!) {
		orderByIdAndToken(_id: $_id, token: $token) {
			_id
			date
			status
			fromCurrency
			toCurrency
			baseRate
			fromAmount
			toAmount
			toBankInfo
			contactEmail
			contactPhone
			contactFullName
			createdAt
			updatedAt
		}
	}
`

export const GET_TRANSACTION_LIST = gql`
	query transactionList(
		$filters: InputTransactionFilters
	) {
		transactionsList (
			filters: $filters
		) {
			hasNext
			nextPage
			pages
			total
			items {
				_id
				type
				rate
				currency
				currencyLabel
				amountBtc
				amountCurrency
				paymentMethod
				paymentMethodDescription
				images
				# comments {
				# 	body
				# 	userId
				# 	user {
				# 	firstName
				# 	lastName
				# 	email
				# 	}
				# }
				user {
					firstName
					lastName
					email
				}
				userId
				createdAt
				updatedAt
			}
		}
	}
`

export const GET_TRANSACTION = gql`
	query transactionGet($_id: String!) {
		transactionGet(_id: $_id) {
			_id
			type
			rate
			currency
			currencyLabel
			amountBtc
			amountCurrency
			paymentMethod
			paymentMethodDescription
			images
			bankTransfers {
				_id
				amount
				bankDescription
				currency
				date
				imageUrl
				status
				transactionId
				userId
				createdAt
				updatedAt
			}
			user {
				firstName
				lastName
				email
			}
			userId
			createdAt
			updatedAt
		}
	}
`

export const GET_ARBITRAGE_TRANSACTION_LIST = gql`
	query arbitrageTransactionList($filters: InputArbitrageTransactionFilters) {
		arbitrageTransactionsList(filters: $filters) {
			hasNext
			nextPage
			pages
			total
			items {
				_id
				direction
				service
				buyTransactionId
				sellTransactionId
				orderId
				profitBtc
				profitPercent
				userId
				createdAt
				updatedAt
			}
		}
	}
`

export const GET_ARBITRAGE_TRANSACTION = gql`
	query arbitrageTransactionGet($_id: String!) {
		arbitrageTransactionGet(_id: $_id) {
			_id
			service
			buyTransactionId
			sellTransactionId
			orderId
			profitBtc
			profitPercent
			userId
		}
	}
`

export const GET_BANK_TRANSFER_LIST = gql`
	query bankTransfersList($filters: InputBankTransferFilters) {
		bankTransfersList(filters: $filters) {
			hasNext
			nextPage
			pages
			total
			items {
				_id
				amount
				bankDescription
				currency
				date
				imageUrl
				status
				transactionId
				userId
				createdAt
				updatedAt
			}
		}
	}
`

export const GET_BANK_TRANSFER = gql`
	query bankTransferGet($_id: String!) {
		bankTransferGet(_id: $_id) {
			_id
			amount
			bankDescription
			currency
			date
			imageUrl
			status
			transactionId
			userId
			createdAt
			updatedAt
		}
	}
`