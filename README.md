# react-remesas-app

Remesas App - Prototype for arbitrage, send and receive money

## Technologies

- [React.js v16](https://reactjs.org/)
- [React Redux v7](https://react-redux.js.org/)
- [Redux v4](https://redux.js.org/)
- [Materia UI v4](https://material-ui.com/)
- [Moment.js v2](https://momentjs.com)
- [Formik v2](https://formik.org/)
- [Yup v0.29](https://github.com/jquense/yup)
- [Apollo Client](https://www.apollographql.com/docs/react/get-started/)

## Environment Variables

```
PUBLIC_URL=http://198.74.53.105:3000
```

### File src/config.js

This file is used for defining variables depending on value of NODE_ENV in .env.

## Instalation

You can use `yarn` or `npm` to install the dependencies.

- `yarn install` recommended
- `npm install`

## Deployment

The next instructions has been thought for a server with a Ubuntu server OS or another linux environment.

### Docker

1. `docker build -t react-remesas-app:{version} .`
2. `docker run -d -p 80:80 react-remesas-app:{version}`

You should configurate `nginx-proxy` or `apache-proxy` if you already have a web server configured. Some reference links bellow:

- [Use Ngnix To Proxy Docker Containers On Ubuntu](https://www.hostwinds.com/guide/use-ngnix-to-proxy-docker-containers-on-ubuntu/)
- [How To Run Nginx in a Docker Container on Ubuntu 16.04](https://vexxhost.com/resources/tutorials/how-to-run-nginx-in-a-docker-container-on-ubuntu-16-04/)
- [How To Configure Nginx as a Web Server and Reverse Proxy for Apache on One Ubuntu 20.04 Server](https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-20-04-server)

### GitLab CI

There is a .gitlab-ci.yml file for run a deployment on custom server.

- Enter to the server and create/configure a ssh-key. [Link recommended](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2)
- After creating the key you must add to a ~/.ssh/authorized_keys.
  1. `chmod 644 ~/.ssh/authorized_keys`
	2. `cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys`
- On the GitLab repository page, go to **Settings** -> **CI / CD** -> **Variables**
	1. Add the variable ```SERVIDOR_CI``` with the value of the server's Public IP. Use command `hostname -I` or another command.
	2. Add the variable ```SSH_PRIVATE_KEY``` with the content of SSH private key. Use command `cat ~/.ssh/id_rsa`.
- On the GitLab repository page go to **Settings** -> **Repository** -> **Deploy Keys**:
	1. Add the SSH public key for using git commands on server.

### Heroku

TODO:

### Nginx (html standalone)

1. Follow the steps for installing nginx. [Link of DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-debian-8#step-1-%E2%80%94-install-the-nginx-web-server)
2. Build the application
  - Edit the .env-example file with your domain.
	- Run `npm install && npm run build`
	- A folder `build` has been created.
	- Copy the files contained in `build` folder to `/var/www/html` o whatever your path of your nginx website root.

#### Apache

With apache it sould be the same. Follow the next [link](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-18-04) for installing apache on Ubuntu server.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Developer

- Ramon Serrano <ramon.calle.88@gmail.com>
